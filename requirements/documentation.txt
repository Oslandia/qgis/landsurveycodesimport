# Documentation
# -------------

furo==2022.*
myst-parser[linkify]>=0.15,<0.17
sphinx-autobuild==2021.*
sphinxcontrib-mermaid>=0.6,<0.8
sphinx-copybutton>=0.3,<1
sphinxext-opengraph>=0.4,<1
sphinx-panels>=0.5,<0.7
attrs>=21.2.0

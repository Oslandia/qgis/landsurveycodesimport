# Develop matching the development guidelines
# -------------------------------------------

black
flake8>=4,<4.1
flake8-builtins>=1.5,<1.6
flake8-eradicate>=1.0,<1.3
flake8-isort>=4.0,<4.2
pre-commit>=2.15,<2.17

# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2014 - 2022, Loïc Bartoletti (Oslandia), Julien Moura
# (Oslandia), Jacky Volpes (Oslandia)
# This file is distributed under the same license as the Land Survey Codes
# Import package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Land Survey Codes Import 0.3.0-beta7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-04-19 16:17+0200\n"
"PO-Revision-Date: 2022-04-19 15:02+0000\n"
"Last-Translator: Jacky Volpes <jacky@volpes.fr>\n"
"Language-Team: English <https://hosted.weblate.org/projects/"
"landsurveycodesimport/documentation-developmentdiagram/en/>\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.12-dev\n"
"Generated-By: Babel 2.9.1\n"

#: ../development/diagram.md:1
msgid "Diagramme technique"
msgstr "Technical diagram"

#~ msgid "Technical diagram"
#~ msgstr ""

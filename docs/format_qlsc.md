# QGIS Land Survey Codification (QLSC) - Spécification

Le plugin utilise son propre format d'échange pour stocker les fichiers de codification, baptisé QLSC (`.qlsc`) qui est basé sur le format de fichier [YAML file](https://fr.wikipedia.org/wiki/YAML).

## Implémentation

Les fichiers de codification sont lus et écrits en utilisant l'implémentation YAML en Python (PyYAML, intégré par défaut dans QGIS), en inscrivant les types de données dans les fichiers, comme décrit dans la [documentation de la bibliothèque](https://pyyaml.org/wiki/PyYAMLDocumentation#yaml-tags-and-python-types).

## Structure

Voici la structure type d'un fichier de codification :

```yaml
AllPoints: {Layer: <GEOPACKAGE_PATH>|layername=<LAYERNAME>, isChecked: <true|false>}
CodeSeparator: <A CHARACTER>
Codification:
'<CODE>':
    Attributes:
    - !!python/tuple [<COLUMN_NAME>, <EXPRESSION or special value>]
    Description: '<A DESCRIPTION>'
    GeometryType: '<GEOMTERYTYPE FROM THE VALID TYPE>'
    Layer:<GEOPACKAGE_PATH>|layername=<LAYERNAME>
ErrorPoints: {Layer: <GEOPACKAGE_PATH>|layername=<LAYERNAME>, isChecked: <true|false>}
BoundingGeometry:
    BoundingGeometryType: <CODE_GEOMETRY>
    Layer:<GEOPACKAGE_PATH>|layername=<LAYERNAME>
    isChecked: true
ParameterSeparator: '<A CHARACTER>'
```

- AllPoints : la couche pouvant recevoir tous les points du levé
- CodeSeparator : le caractère permettant de sépararer les codes
- Codification : contient l'ensemble des codes utilisé par votre codification
    suivant cette convention.
    - Attributes : une liste de tuple ( colonne: expression ) permettant
        d'ajouter les attributs dans la table
    - Description : la description du code
    - GeometryType : le type de géométrie (voir la liste disponible)
    - Layer : la couche qui va recevoir la géométrie provenant du levé codifié
- ErrorPoints : la couche pouvant recevoir tous les points en erreur du levé
- BoundingGeometry : information sur la couche recevant le calcul et le type de
    l'emprise englobante
- ParameterSeparator : le caractère permettant de sépararer les paramètres

## Exemple

Des exemples sont disponibles dans le dossier `tests\fixtures` du dépôt. Par exemple, voici le fichier utilisé dans le jeu de données de démonstration :

```{eval-rst}
.. literalinclude:: ../../LandSurveyCodesImport/resources/demo/codification.qlsc
  :language: yaml
```

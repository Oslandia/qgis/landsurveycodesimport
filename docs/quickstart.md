# Didacticiel de démarrage rapide

Afin de démarrer rapidement, un jeu de données et un projet de démonstration sont livrés avec le plugin et accessibles via le menu idoine :

![Demonstration load menu](../_static/images/fr/demo_load.png "Demonstration load menu")

Les données et le projet de démonstration sont alors copiés dans un dossier temporaire, avant d'être chargés dans QGIS.

Il est ensuite possible de manipuler les différents outils du plugin. Par exemple, ouvrir le fichier de codification :

<video width="700" controls>
    <!-- markdownlint-disable MD033 -->
      <source src="https://gitlab.com/Oslandia/qgis/landsurveycodesimport/uploads/541619199c2c72d81833c3f7474150fb/lsci_demonstration_load.mp4" type="video/mp4">
      Votre navigateur ne supporte pas la balise video HTML 5.
      <!-- markdownlint-enable MD033 -->
</video>

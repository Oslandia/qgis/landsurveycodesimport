# Glossaire

## Carnet de terrain

Instrument nomade (généralement électronique désormais) utilisé pour noter les données recueillies sur un terrain d'étude et susceptibles d'être interprétées. En ce sens, le carnet de terrain est un outil permettant de systématiser les expériences pour pouvoir analyser les résultats par la suite.

## Codification topographique

Un levé codifié est un levé topographique (réalisé avec une station totale ou un GPS) où à chaque point est associé un ou plusieurs codes dans le carnet de terrain. La codification permet d'affecter automatiquement les objets dessinés à la bonne couche de dessin.

## GeoPackage

Le choix a été fait d'utiliser le GeoPackage (`.gpkg`) comme format central du plugin pour plusieurs raisons :

- format standardisé OGC (contrairement à [SpatiaLite]) ;
- base de données stockée sous forme d'un seul fichier à plat (_flat database_) qui permet de stocker plusieurs jeux de données ;
- possibilité de stocker des éléments en plus des données géographiques : projet QGIS, styles QGIS, etc. La logique est donc proche de celle des métiers du DAO[^1] qui ont l'habitude de disposer des données et du dessin ;
- format par défaut de QGIS, incluant donc une excellente intégration dans les API et outils disponibles par ailleurs dans le logiciel.

Dans le plugin, il est utilisé comme format de stockage principal et également comme format pivot. Il permet un archivage des données (plan de topographie d'un secteur par exemple).

## Levé codifié

Un levé codifié est un levé topographique (station totale ou GPS) où à chaque point est associé un ou plusieurs codes dans le carnet de terrain. Contrairement à un "simple" relevé via un carnet terrain, se fait à l'aide d'appareils de précision (station totale [théodolite](https://fr.wikipedia.org/wiki/Th%C3%A9odolite)).

La codification permet de dessiner automatiquement les objets dans différentes couches du dessin.

Voir aussi le [levé topographique sur Wikipédia](https://fr.wikipedia.org/wiki/Lever_topographique).

## Topométrie

Ensemble des moyens géométriques employés pour effectuer des mesures de positions relatives de points. C’est donc la boîte à outils de base du topographe.

<!-- Footnotes -->
[^1]: Dessin Assisté par Ordinateur

<!-- Hyperlinks reference -->
[SpatiaLite]: https://www.gaia-gis.it/fossil/libspatialite/home

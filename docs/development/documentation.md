# Documentation

Ce projet utilise Sphinx pour générer la documentation à partir de docstrings (documentation dans le code) et des pages écrites en Markdown (avec [MyST parser](https://myst-parser.readthedocs.io/en/latest/)).

## Construire la documentation HTML

```bash
# installer les dépendances additionnelles
python -m pip install -U -r requirements/documentation.txt
# build it
sphinx-build -b html docs docs/_build
```

Ouvrir `docs/_build/index.html` dans un navigateur web.

## Écrire la documentation en utilisant le rendu en direct

```bash
sphinx-autobuild -b html docs/ docs/_build
```

Ouvrir <http://localhost:8000> dans un navigateur web pour voir le rendu HTML mis à jour à chaque fois qu'un fichier est changé et sauvegardé.

## Ajouter une nouvelle page à la documentation

Par exemple pour ajouter une page "Super astuces !" :

1. Créer un fichier markdown `docs/super_astuces.md`
2. Référencer cette page dans la table des matières de `docs/index.html`

````markdown
```{toctree}
---
caption: Table des matières
maxdepth: 3
---
[...les autres pages...]
docs/super_astuces
```
````

## Contribuer à une traduction en cours/existante

Aller sur la page du [projet de traduction Weblate](https://hosted.weblate.org/projects/landsurveycodesimport) et choisir une page de la documentation à traduire.

## Ajouter une nouvelle traduction à la documentation

Aller sur la page du [projet de traduction Weblate](https://hosted.weblate.org/projects/landsurveycodesimport/documentation) et cliquer sur "Démarrer une nouvelle traduction".

Adapter le fichier `docs/_templates/sidebar/navigation.html.ref` en ajoutant le lien adapté dans la barre de navigation et le calcul adapté dans le script javascript.

Adapter le fichier `docs/multi_lang_build.sh` en ajoutant la langue en plus.

Par exemple, pour ajouter une traduction `us` :

Ajouter la ligne     

```html
<li class="toctree-l1"><a id="us-link" class="reference internal" href="#">English (US)</a></li>
```

dans la barre de navigation et la ligne

```javascript
document.getElementById('us-link').href = "{{prefix}}/us" + newPath;
```

dans le script et la ligne

```sh
sphinx-build -D language=us $1 $2/us
```
dans le script de construction.

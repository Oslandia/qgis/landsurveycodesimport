# Comment ajouter un nouveau type de géométrie ?

## Développement

- 1. Ajouter la géométrie dans `LandSurveyCodesImport/available_codes.py`
- 2. Ajouter la conversion des points vers la géométrie dans `LandSurveyCodesImport/processing/codification/geom_from_points.py`
- 3. Écrire un test dédié dans `tests/fixtures`

## Documentation

1. Ajouter la géométrie dans le projet `docs/_static/illustration/illustration.qgz` et dans le fichier GeoPackage et placer le nouveau svg dans `docs/_static/images/geometry`
2. Expliquer la codification de cette géométrie dans la documentation

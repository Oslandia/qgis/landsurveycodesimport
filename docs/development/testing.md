# Tests unitaires

## Prérequis

- QGIS 3.16+

```bash
# creer un environment virtuel qui soit lie au paquets systeme (pour avoir acces a pyqgis)
python3 -m venv .venv --system-site-packages

# y installer les dependances
python -m pip install -U pip
python -m pip install -U -r requirements/testing.txt
```

## Lancer les tests unitaires

```bash
# lancer tous les tests avec PyTest et le rapport de couverture
python -m pytest

# lancer le test d'un module specifique avec unittest
python -m unittest tests.test_qlsc_reader

# lancer une fonction de test specifique avec unittest
python -m unittest tests.test_qlsc_reader.TestQlscReader.test_qlsc_reader
```

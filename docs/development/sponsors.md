# Financement

```{admonition} Prêt à contribuer ?
Ce plugin est libre d'utilisation. Si vous l'utilisez de manière intensive ou êtes intéressés pour l'améliorer, merci de contribuer au code, à la documentation, ou 
de financer des développements :

- [Améliorations identfiées](https://gitlab.com/Oslandia/qgis/landsurveycodesimport/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=enhancement)
- Envie de financer ? Merci de [nous envoyer un email](mailto:qgis@oslandia.com)
```

## Sponsors

Merci aux sponsors :

```{eval-rst}
.. panels::
    :card: shadow
    :container: container-lg pb-4
    :column: col-lg-4 col-md-4 col-sm-6 col-xs-12 p-2
    :img-top-cls: pl-5 pr-5

    ---
    :img-top: ../../LandSurveyCodesImport/resources/images/logo_qwat.png

    Groupe d'utilisateurs QWAT
    ^^^^^^^^^^^^^^^^^^^^^^^^^^

    Financement dans le cadre des travaux de QWAT

    ++++++
    .. link-button:: https://qwat.github.io/docs/
        :type: url
        :text: Website
        :classes: btn-outline-primary btn-block

    ---
    :img-top: ../../LandSurveyCodesImport/resources/images/logo_oslandia.png

    Oslandia
    ^^^^^^^^

    Depuis que Loïc Bartoletti a rejoint l'entreprise, Oslandia a contribué au développement du plugin avec ses fonds propres et
    a mis son expertise au service des besoins exprimés par les utilisateurs.

    ++++++
    .. link-button:: https://oslandia.com/
        :type: url
        :text: Website
        :classes: btn-outline-primary btn-block

    ---
    :img-top: ../../LandSurveyCodesImport/resources/images/logo_megeve.jpg

    Megève
    ^^^^^^

    La commune a financé le développement initial à travers son employé Loïc Bartoletti.

    ++++++
    .. link-button:: https://mairie.megeve.fr/
        :type: url
        :text: Website
        :classes: btn-outline-primary btn-block
```

# Packaging et déploiement

## Packaging

Ce plugin utilise l'outil [qgis-plugin-ci](https://github.com/opengisch/qgis-plugin-ci/) pour le packaging et le déploiement.

Exemple :

```bash
qgis-plugin-ci release 0.3.1
```

## Déployer une version

### En utilisant l'intégration continue GitLab

1. Renseigner le `CHANGELOG.md`
2. Appliquer un tag git avec la version associée: `git tag -a 0.3.0 {git commit hash} -m "September 2019"`
3. Pousser le tag sur master

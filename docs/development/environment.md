# Développement

## Mise en place de l'environment

Typiquement sur Ubuntu

```bash
# creer un environment virtuel qui soit lie au paquets systeme (pour avoir acces a pyqgis)
python3 -m venv .venv --system-site-packages

# y installer les dependances
python -m pip install -U pip
python -m pip install -U -r requirements/development.txt

# installer les git hooks (pre-commit)
pre-commit install
```

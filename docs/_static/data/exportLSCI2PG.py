"""
Model exported as python.
Name : qwat
Group : pg_lsci
With QGIS : 31801
"""
import tempfile
from pathlib import Path

import processing
import yaml
from qgis.core import (
    QgsProcessing,
    QgsProcessingAlgorithm,
    QgsProcessingMultiStepFeedback,
    QgsProcessingParameterFile,
    QgsProcessingParameterFileDestination,
    QgsProcessingParameterFolderDestination,
)


class Qwat(QgsProcessingAlgorithm):
    def initAlgorithm(self, config=None):
        self.addParameter(
            QgsProcessingParameterFile(
                "pgtogpkgyaml",
                "pg to gpkg yaml",
                behavior=QgsProcessingParameterFile.File,
                fileFilter="YAML Files (*.yaml)",
                defaultValue=None,
            )
        )
        self.addParameter(
            QgsProcessingParameterFile(
                "fieldbookcsv",
                "fieldbook csv",
                behavior=QgsProcessingParameterFile.File,
                fileFilter="CSV Files (*.csv)",
                defaultValue=None,
            )
        )
        self.addParameter(
            QgsProcessingParameterFile(
                "codification",
                "codification",
                behavior=QgsProcessingParameterFile.File,
                fileFilter="QLSC Files (*.qlsc)",
                defaultValue=None,
            )
        )
        self.addParameter(
            QgsProcessingParameterFolderDestination(
                "Archive", "archive", createByDefault=True, defaultValue=None
            )
        )
        self.addParameter(
            QgsProcessingParameterFileDestination(
                "LogFile",
                "log file",
                optional=True,
                fileFilter="log",
                createByDefault=True,
                defaultValue=None,
            )
        )

    def processAlgorithm(self, parameters, context, model_feedback):
        # Use a multi-step feedback, so that individual child algorithm progress reports are adjusted for the
        # overall progress through the model
        feedback = QgsProcessingMultiStepFeedback(2, model_feedback)
        results = {}
        outputs = {}

        # Import land survey fields
        alg_params = {
            "INPUT": parameters["fieldbookcsv"],
            "LEVELLOG": 3,
            "QLSC": parameters["codification"],
            "DEST": parameters["Archive"],
            "LOG": parameters["LogFile"],
        }
        outputs["ImportLandSurveyFields"] = processing.run(
            "landsurvey:importlsci",
            alg_params,
            context=context,
            feedback=feedback,
            is_child_algorithm=True,
        )
        print(outputs["ImportLandSurveyFields"])

        results["Archive"] = outputs["ImportLandSurveyFields"]["OUTPUT"]
        results["LogFile"] = outputs["ImportLandSurveyFields"]["LOG"]

        feedback.setCurrentStep(1)
        if feedback.isCanceled():
            return {}

        # replace path to archive one
        with open(parameters["pgtogpkgyaml"], "r") as yamlfile:
            data = yaml.load(yamlfile, Loader=yaml.FullLoader)

        for d in data.keys():
            data[d]["gpkg_file"] = str(
                Path(results["Archive"]) / Path(data[d]["gpkg_file"]).name
            )

        with tempfile.NamedTemporaryFile(suffix=".yaml") as yaml_temp:
            with open(yaml_temp.name, "w") as f:
                dump = yaml.dump(data, f)
            # end replace path to archive one
            print(data)
            # Export pg to gpkg
            alg_params = {"INPUT": yaml_temp.name, "PROJECT": None, "TRUNC": False}

            outputs["ExportGpkgToPg"] = processing.run(
                "ExportDBMapper:Exportgpkg2pg",
                alg_params,
                context=context,
                feedback=feedback,
                is_child_algorithm=True,
            )
            return results

    def name(self):
        return "exportFBtoPG"

    def displayName(self):
        return "Export Field book to PostgreSQL"

    def group(self):
        return "lsci"

    def groupId(self):
        return "lsci"

    def createInstance(self):
        return Qwat()

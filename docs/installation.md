# Installation

## Pré-requis

- QGIS {{ qgis_version_min }}
- certains packages Python listés dans `requirements/base.txt` (normalement intégrés par défaut dans QGIS) :

Par exemple sur Ubuntu LTS:

```bash
python3 -m pip install -U -r requirements/base.txt
```

## Version stable (recommandée)

Le plugin est publié sur le dépôt officiel des extensions de QGIS : <https://plugins.qgis.org/plugins/LandSurveyCodesImport/>.

## Versions expérimentales

Des versions intermédiaires (alpha, beta...) sont parfois publiées sur le dépôt officiel dans le canal expérimental.

Pour y accéder, il suffit d'activer les extensions expérimentales dans les préférences du gestionnaire d'extensions de QGIS.

## Version en développement

Si vous vous considérez comme un *early adopter*, un testeur ou que vous ne pouvez attendre qu'une version soit publiée (même dans le canal expérimental !), vous pouvez utiliser la version automatiquement packagée pour chaque commit poussé sur la branche principale.

Pour cela, il faut ajouter cette URL dans les dépôts référencés dans le gestionnaire d'extensions de QGIS :

```html
https://oslandia.gitlab.io/qgis/landsurveycodesimport/plugins.xml
```

Attention, cette version peut être instable.

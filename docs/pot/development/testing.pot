# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2014 - 2022, Loïc Bartoletti (Oslandia), Julien Moura (Oslandia), Jacky Volpes (Oslandia)
# This file is distributed under the same license as the Land Survey Codes Import package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Land Survey Codes Import 0.3.0-beta7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-04-19 16:17+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../development/testing.md:1
msgid "Tests unitaires"
msgstr ""

#: ../development/testing.md:3
msgid "Prérequis"
msgstr ""

#: ../development/testing.md:5
msgid "QGIS 3.16+"
msgstr ""

#: ../development/testing.md:16
msgid "Lancer les tests unitaires"
msgstr ""

# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2014 - 2022, Loïc Bartoletti (Oslandia), Julien Moura (Oslandia), Jacky Volpes (Oslandia)
# This file is distributed under the same license as the Land Survey Codes Import package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Land Survey Codes Import 0.3.0-beta7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-04-19 11:44+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../CHANGELOG.md:1
msgid "Changelog"
msgstr ""

#: ../../CHANGELOG.md:3
msgid "[0.3.0-beta7] - 2021/06/24"
msgstr ""

#: ../../CHANGELOG.md:5
msgid "Fixes logger error"
msgstr ""

#: ../../CHANGELOG.md:6
msgid "Adds a new processing to export fieldbook in a PostgreSQL DB (This tool requires ExportDBMapper plugin)"
msgstr ""

#: ../../CHANGELOG.md:7
#: ../../CHANGELOG.md:15
msgid "Improved documentation"
msgstr ""

#: ../../CHANGELOG.md:9
msgid "[0.3.0-beta6] - 2021/04/28"
msgstr ""

#: ../../CHANGELOG.md:11
msgid "Fixes missing resources_rc"
msgstr ""

#: ../../CHANGELOG.md:13
msgid "[0.3.0-beta5] - 2021/04/28"
msgstr ""

#: ../../CHANGELOG.md:16
msgid "Improved logging function"
msgstr ""

#: ../../CHANGELOG.md:17
msgid "Adds demonstration and quickstart"
msgstr ""

#: ../../CHANGELOG.md:18
msgid "Adds UI shortcuts"
msgstr ""

#: ../../CHANGELOG.md:19
msgid "Fixes processing translation"
msgstr ""

#: ../../CHANGELOG.md:21
msgid "[0.3.0-beta3] - 2021/02/09"
msgstr ""

#: ../../CHANGELOG.md:23
msgid "Fix automatic deployment from GitLab CI"
msgstr ""

#: ../../CHANGELOG.md:25
msgid "[0.3.0-beta2] - 2021/02/08"
msgstr ""

#: ../../CHANGELOG.md:27
msgid "Documentation refactoring"
msgstr ""

#: ../../CHANGELOG.md:28
msgid "Use modern workflow to package and release the plugin"
msgstr ""

#: ../../CHANGELOG.md:29
msgid "Minor code changes"
msgstr ""

#: ../../CHANGELOG.md:31
msgid "[0.3.0-beta1] - 2020/12/23"
msgstr ""

#: ../../CHANGELOG.md:33
msgid "Improve code quality"
msgstr ""

#: ../../CHANGELOG.md:34
msgid "Adds an archiving function: Now, the plugin no longer works directly on the input data but makes a copy. This avoids having to modify the input data."
msgstr ""

#: ../../CHANGELOG.md:35
msgid "Adds logging function (optional)"
msgstr ""

#: ../../CHANGELOG.md:37
msgid "0.2.2 - 2020/08/26"
msgstr ""

#: ../../CHANGELOG.md:39
msgid "Add the ability to generate a minimal bouding geometry (convex, oriented minimum bounding box and minimal circle) from all points"
msgstr ""

#: ../../CHANGELOG.md:41
msgid "0.2.1 - 2020/02/10"
msgstr ""

#: ../../CHANGELOG.md:43
msgid "Export LandSurveyCodesImport codification file (.qlsc) to PDF"
msgstr ""

#: ../../CHANGELOG.md:44
msgid "Improve Trimble JobXML to csv (Use all fields, can export deleted points)"
msgstr ""

#: ../../CHANGELOG.md:46
msgid "0.2.0 - 2020/08/19"
msgstr ""

#: ../../CHANGELOG.md:48
msgid "Add support to arc for line"
msgstr ""

#: ../../CHANGELOG.md:49
msgid "Add support to rectangle/square => Minimal version is now QGIS 3.6"
msgstr ""

#: ../../CHANGELOG.md:50
msgid "Import Trimble JobXML to LandSurveyCodesImport csv"
msgstr ""

#: ../../CHANGELOG.md:51
msgid "Export LandSurveyCodesImport codification file (.qlsc) to csv"
msgstr ""

#: ../../CHANGELOG.md:53
msgid "0.1.1 - 2020/07/15"
msgstr ""

#: ../../CHANGELOG.md:55
msgid "Fix incorrect ownership of algorithms in processing provider"
msgstr ""

#: ../../CHANGELOG.md:56
msgid "Remove experimental flag"
msgstr ""

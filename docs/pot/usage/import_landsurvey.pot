# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2014 - 2022, Loïc Bartoletti (Oslandia), Julien Moura (Oslandia), Jacky Volpes (Oslandia)
# This file is distributed under the same license as the Land Survey Codes Import package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Land Survey Codes Import 0.3.0-beta7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-04-19 11:44+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../usage/import_landsurvey.md:1
msgid "Import du carnet de terrain"
msgstr ""

#: ../usage/import_landsurvey.md:3
msgid "De retour du terrain, le plugin propose un outil d'import pour intégrer automatiquement le carnet de terrain :"
msgstr ""

#: ../usage/import_landsurvey.md:5
msgid "![processing_importlsci](/_static/images/fr/processing_importlsci.png)"
msgstr ""

#: ../usage/import_landsurvey.md:5
msgid "processing_importlsci"
msgstr ""

#: ../usage/import_landsurvey.md:7
msgid "Options"
msgstr ""

#: ../usage/import_landsurvey.md:9
msgid "L'import se fait en sélectionnant :"
msgstr ""

#: ../usage/import_landsurvey.md:11
msgid "le fichier de codification utilisé pour l'enquête terrain (`.qlsc`)"
msgstr ""

#: ../usage/import_landsurvey.md:12
msgid "le fichier CSV avec les données de l'enquête terrain"
msgstr ""

#: ../usage/import_landsurvey.md:13
msgid "le répertoire d'archivage (là où va se réaliser l'export)"
msgstr ""

#: ../usage/import_landsurvey.md:14
msgid "le niveau de journalisation (log)"
msgstr ""

#: ../usage/import_landsurvey.md:15
msgid "le fichier de journalisation (log)"
msgstr ""

#: ../usage/import_landsurvey.md:17
msgid "![importlsci_windows](/_static/images/fr/importlsci_window.png)"
msgstr ""

#: ../usage/import_landsurvey.md:17
msgid "importlsci_windows"
msgstr ""

#: ../usage/import_landsurvey.md:19
msgid "Traitement"
msgstr ""

#: ../usage/import_landsurvey.md:21
msgid "Le plugin copie les [GeoPackage] sources dans le répertoire d'archivage. Si aucun chemin n'est indiqué QGIS crée un dossier temporaire avant d'y copier les données dedans."
msgstr ""

#: ../usage/import_landsurvey.md:23
msgid "LSCI[^lsci] vous propose également de logguer ces différentes étapes, particulièrement utiles pour les développeurs en cas de problème (`DEBUG`), mais également pour vous (`INFO`) pour vous indiquer ce qui a été réalisé et s'il y a des erreurs dans votre codification."
msgstr ""

#: ../usage/import_landsurvey.md:25
msgid "Résultat en sortie"
msgstr ""

#: ../usage/import_landsurvey.md:27
msgid "![importlsci_windows_result](/_static/images/fr/importlsci_window_result.png)"
msgstr ""

#: ../usage/import_landsurvey.md:27
msgid "importlsci_windows_result"
msgstr ""

#: ../usage/import_landsurvey.md:29
msgid "Les données du terrain sont chargées dans vos différents [GeoPackage] et vous pouvez admirer votre résultat en chargeant votre projet :"
msgstr ""

#: ../usage/import_landsurvey.md:31
msgid "![result](/_static/images/result.png)"
msgstr ""

#: ../usage/import_landsurvey.md:31
msgid "result"
msgstr ""

#: ../usage/import_landsurvey.md:33
msgid "Vous avez la possibilité de refaire le traitement autant de fois que vous le souhaitez. si vous constatez une erreur, vous pouvez donc corriger manuellement votre dessin ou reprendre votre fichier terrain et le réimporter."
msgstr ""

#: ../usage/import_landsurvey.md:1
msgid "Comme le plugin est intégré dans QGIS et notamment les traitements, il est possible de chaîner l'import avec d'autres traitements."
msgstr ""

#: ../usage/import_landsurvey.md:41
msgid "Principe du traitement du CSV vers vos GeoPackage"
msgstr ""

#: ../usage/import_landsurvey.md:43
msgid "Cette partie détaille le fonctionnement technique du plugin. Elle n'est pas nécessaire pour l'utiliser."
msgstr ""

#: ../usage/import_landsurvey.md:45
msgid "Séparation des codes"
msgstr ""

#: ../usage/import_landsurvey.md:47
msgid "L’outil va séparer les codes qui sont sur un même point, pour ensuite les apparier suivant leur code. Par exemple un CSV comme :"
msgstr ""

#: ../usage/import_landsurvey.md:55
msgid "sera transformé en :"
msgstr ""

#: ../usage/import_landsurvey.md:65
msgid "puis :"
msgstr ""

#: ../usage/import_landsurvey.md:75
msgid "L’outil traite les codes à la file, une attention particulière est demandée à l’opérateur saisissant les codes car une erreur de saisie décalera la codification et le dessin en résultant."
msgstr ""

#: ../usage/import_landsurvey.md:77
msgid "Conversion des points en géométries"
msgstr ""

#: ../usage/import_landsurvey.md:79
msgid "Une fois le CSV traité en interne, le plugin va le lire séquentiellement et convertit les points en géométries suivant le code."
msgstr ""

#: ../usage/import_landsurvey.md:81
msgid "Format"
msgstr ""

#: ../usage/import_landsurvey.md:83
msgid "À ce jour, le plugin ne sait lire que les fichiers de points au format CSV. Celui-ci est souvent présent sur les appareils. Ils devront respecter cette disposition :"
msgstr ""

#: ../usage/import_landsurvey.md:85
msgid "`numéro,x,y,z,code,att1,att2, [...] attn`"
msgstr ""

#: ../usage/import_landsurvey.md:87
msgid "où :"
msgstr ""

#: ../usage/import_landsurvey.md:89
msgid "`numéro` est le numéro du point,"
msgstr ""

#: ../usage/import_landsurvey.md:90
msgid "`x`, `y` et `z` les coordonnées,"
msgstr ""

#: ../usage/import_landsurvey.md:91
msgid "`code` le(s) code(s) sur le point,"
msgstr ""

#: ../usage/import_landsurvey.md:92
msgid "`att1`, `att2` et `attn` sont les attributs pouvant être exportés de votre carnet de terrain. Il n'y a pas de limite sur le nombre d'attribut."
msgstr ""

#: ../usage/import_landsurvey.md:95
msgid "acronyme du nom du plugin Land Survey Codes Import"
msgstr ""

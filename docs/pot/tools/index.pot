# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2014 - 2022, Loïc Bartoletti (Oslandia), Julien Moura (Oslandia), Jacky Volpes (Oslandia)
# This file is distributed under the same license as the Land Survey Codes Import package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Land Survey Codes Import 0.3.0-beta7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-04-19 11:44+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../tools/index.md:1
msgid "Outils de traitement fournis avec le plugin"
msgstr ""

#: ../tools/index.md:3
msgid "En plus de la gestion de la codifiation, différents utilitaires sont fournis avec le plugin sous forme de traitements QGIS. Ils sont donc accessibles dans [la boîte à outils des traitements](https://docs.qgis.org/3.16/fr/docs/user_manual/processing/toolbox.html) intégrée via le menu `Traitement` du menu horizontal de QGIS."
msgstr ""

#: ../tools/index.md:6
msgid "![QGIS Processing toolbox](/_static/images/fr/qgis_processing_toolbox_lsci.png \"QGIS Processing Toolbox - Land Survey Codes Import\")"
msgstr ""

#: ../tools/index.md:6
msgid "QGIS Processing toolbox"
msgstr ""

#: ../tools/index.md:8
msgid "Par commodité, les outils sont également accessibles directement depuis les menus du plugin :"
msgstr ""

#: ../tools/index.md:10
msgid "![QGIS menu plugin LSCI](/_static/images/fr/lsci_menu.png \"menu du plugin QGIS LSCI\")"
msgstr ""

#: ../tools/index.md:10
msgid "QGIS menu plugin LSCI"
msgstr ""

#: ../tools/index.md:12
msgid "Outils"
msgstr ""

# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2014 - 2022, Loïc Bartoletti (Oslandia), Julien Moura (Oslandia), Jacky Volpes (Oslandia)
# This file is distributed under the same license as the Land Survey Codes Import package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Land Survey Codes Import 0.3.0-beta7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-04-19 11:44+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../misc/faq.md:1
msgid "Questions fréquentes"
msgstr ""

#: ../misc/faq.md:3
msgid "Le CSV n'est pas compatible avec celui de LSCI ?"
msgstr ""

#: ../misc/faq.md:5
msgid "Aucun problème, vous pouvez le convertir en utilisant QGIS ou de nombreux outils."
msgstr ""

#: ../misc/faq.md:7
msgid "Si vous avez déjà développé vos scripts ou modèles QGIS, ou si vous souhaitez nous financer pour le faire, [contactez-nous](mailto:infos@oslandia.com?subject=LSCI%20plugin)."
msgstr ""

#: ../misc/faq.md:9
msgid "Quelle différence avec des solutions comme Input ou QField ?"
msgstr ""

#: ../misc/faq.md:11
msgid "[Input] et [QField] sont deux solutions embarquées permettant de relever et éditer des données sur le terrain. Ils nécessitent l'installation d'une application sur une tablette ou smartphone. LSCI[^lsci] ne nécessite que l'utilisation du [carnet de terrain] qui équipe votre appareil de topographie (station totale ou GNSS) comme un Trimble TSC3, Leica CS20, etc."
msgstr ""

#: ../misc/faq.md:13
msgid "Ce plugin s'adresse aux brigades de topographie habituée à ce genre de levé codifié ou ceux soucieux d'optimiser le relevé de terrain grâce à cette méthode."
msgstr ""

#: ../misc/faq.md:17
msgid "acronyme du nom du plugin Land Survey Codes Import"
msgstr ""

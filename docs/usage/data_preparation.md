# Préparer ses données

Le [GeoPackage] est le format central.
Nous préconisons de travailler sur une copie vierge de vos données, ne contenant que la structure de vos tables.
Cela permet d'insérer vos données de terrains, d'archiver les données par plans et ensuite de les insérer dans votre base de production.

# Comment préparer vos données

Vous trouverez ci-après quelques cas d'utilisations et conseils pour profiter
pleinement du plugin. Ces conseils sont à adapter à votre processus de travail.

```{note}
Les parties ci-après n'ont pas pour objet de se substituer au manuel de QGIS et
une formation SIG. Ils n'ont pour vocation que de donner des pistes pour
vous assister à la préparation de la donnée pour ce plugin. À vous de les
adapter à votre environnement de travail.
```
En préambule, nous tenons à rappeler pourquoi, nous n'utilisons pas directement
les données PostgreSQL/PostGIS. Ces informations proviennent d'expérience
utilisateurs et de discussions entre eux et les développeurs.

- L'intégration des points topographiques, peuvent parfois engendrer des
    décalages, des corrections à reprendre, etc. Il n'est pas préférable
    d'intégrer directement ceux-ci dans la base de production.
- Nous avons la volonté de conserver un fichier dessin temporaire pour le
    suivi des chantiers. C'est ainsi qu'intervient la notion d'archivage que
    vous verrez dans l'étape suivante.
- La donnée saisie sur le terrain est souvent "brut" et va repasser par un
    cycle d'enrichissement automatique (ce que fait en partie le plugin) ou
    manuel. Ces étapes pouvant être chaînées dans QGIS.
- De même certaines structures vont contrôler le fichier dessin produit par
    l'opérateur terrain et le fournir à une autre entité pour l'intégration
    dans la base.
- Le GPKG en tant que fichier plat, permet également le découpage entre les
    acteurs/métiers comme dans la démonstration : un fichier par métier.
    Ce qui permet une plus grande flexibilité dans le processus terrain.
- Certaines structures passent par des schémas "temporaires" pour l'intégration
- Il y a une différence de schéma (simplification ?) entre les données à
    récolter sur le terrain et celles en base
- Vous pouvez ne pas avoir de base PostgreSQL

Le plugin reprend la logique d'autres outils métiers où un template (le GPKG vide)
sera utilisé pour recevoir la donnée terrain.
Une fois le nouveau GPKG créé, celui-ci peut être intégré au plan d'ensemble.
Nous retrouvons la même logique que la réalisation d'un DWG en DAO et
intégration dans le plan d'ensemble DWG (ou dans postgis).

Une fois les données acquises et traitées avec le plugin, vous obtenez une
archive de ces dessins que vous pouvez rebasculer dans votre base.

## Votre SIG est composé de Shapefile ?

QGIS vous permet de convertir vos fichiers dans différents formats dont le
[GeoPackage]. Plusieurs moyens peuvent être mis en œuvre pour y parvenir.

Pour cela, vous pouvez :
- enregistrer les fichiers en utilisant le menu "Exporter->Sauvegarder les entités sous"
- utiliser le plugin QConsolidate qui permet d'exporter chaque fichier présent
    dans le projet dans un GeoPackage différent et crée un nouveau projet QGIS
    contenant ces nouvelles couches
- utiliser la commande de conversion
    [ogr2ogr](https://gdal.org/programs/ogr2ogr.html). Celle-ci étant présente
    dans l'outil de traitement de QGIS et possède l'avantage de pouvoir
    spécifier des options de conversions. C'est la méthode que nous préconisons.

### Utilisation de ogr2ogr via QGIS

- ouvrez l'outil [Conversion de
format](https://docs.qgis.org/3.16/fr/docs/user_manual/processing_algs/gdal/vectorconversion.html#gdalconvertformat)
dans la boîte à outils de traitements.
- sélectionnez vos Shapefiles en entrée et indiquez vos [GeoPackage] en sortie.

Notez sur la capture ci-dessous, qu'une colonne supplémentaire est présente.
Elle a été affichée en utilisant la roue crantée. Elle permet de définir une
option, ici '-append'. Celle-ci vous permettra d'ajouter plusieurs tables dans
un seul et même [GeoPackage] comme c'est le cas dans notre démonstration.

Pour vider les tables et n'avoir que la donnée vide, ce que nous préconisons,
pour l'utilisation de l'archivage par la suite, vous pouvez ajouter l'option
'-limit 0'

```{note}
Pour plus d'informations nous vous laissons vous reporter au manuel de ogr2ogr.
```

![ogr2ogr](/_static/images/fr/ogr2ogr.png)

Vos fichiers sont convertis en [GeoPackage], vous pouvez les importer dans un
nouveau projet QGIS et réaliser l'étape de création de votre codification.

<!--
```{note}
Vous disposez d'une [archive zip](https://gitlab.com/Oslandia/qgis/landsurveycodesimport/-/blob/master/docs/_static/data.zip)
sur le dépôt du plugin. Vous pouvez utiliser le projet QGIS se trouvant dans le
répertoire shp et charger le fichier export_to_gpkg_with_ogr.json dans l'outil
de traitement.
Attention vous devrez adapter les chemins à votre environnement, il est probable
que le dossier de sortie /tmp/lsci n'existe pas chez vous.
```
-->

## Votre SIG est composé de fichier GeoPackage ?

Vous pouvez, soit procéder comme pour la conversion des Shapefiles en utilisant
l'outil de conversion de format, soit copier vos [GeoPackage] dans un dossier et
ensuite utiliser l'outil permettant de [vider les tables](https://docs.qgis.org/3.16/fr/docs/user_manual/processing_algs/qgis/vectorgeneral.html#qgistruncatetable).

## Votre SIG est dans une base PostgreSQL ?

Comme pour le [GeoPackage] ou le [Shapefile] vous pouvez utiliser l'outil de
conversion de format.

Il existe également d'autres plugins pour la conversion de PostgreSQL vers du
[GeoPackage]. Notre préconisation porte sur le plugin
[ExportDBMapper](https://gitlab.com/Oslandia/qgis/exportdbmapper) que nous avons
développé pour l'occasion.

À l'inverse d'autres plugins, celui-ci a la particularité de pouvoir s'intégrer
totalement dans une chaîne de traitement QGIS. Ce plugin est en quelque sorte
une fusion du meilleur de QConsolidate et d'ogr2ogr.

```{note}
La documentation du projet est disponible à cette
[page](https://oslandia.gitlab.io/qgis/exportdbmapper/fr_guide/1-usage.html).
```


### Étape d'export

Vous définissez l'export de vos données PSQL en GPKG et vous obtenez un fichier
YAML qui vous sera utile pour réaliser cette conversion avec la possibilité en
plus d'avoir un fichier de projet QGIS.

Vous pouvez ouvrir le projet QGIS et créer votre codification comme indiqué dans
l'étape suivante.

### Étape d'import

Pour les personnes désirant recharger automatiquement la donnée traitée par LSCI
dans une base PostgreSQL il vous suffira de créer le fichier de configuration
YAML de la phase d'export.

Vous trouverez sur notre dépôt un [exemple de script]( https://gitlab.com/Oslandia/qgis/landsurveycodesimport/-/blob/master/docs/_static/exportLSCI2PG.py) permettant de réaliser ce
genre de traitement. Celui-ci est à adapter à votre environnement.

```{note}
LandSurveyCodesImport est une "brique" générique qui s'intègre pleinement dans
la chaîne de traitement de QGIS. Libre à vous de configurer les post- et pré-
traitements pour l'automatisation de l'intégration de vos données topographique.
Si vous souhaitez bénéficier d'une assiste ou d'une formation sur ces sujets,
[contactez-nous](mailto:infos@oslandia.com?subject=LSCI%20plugin).
```

<!-- Hyperlinks reference -->
[GeoPackage]: ../glossary.html#geopackage

# Import du carnet de terrain

De retour du terrain, le plugin propose un outil d'import pour intégrer automatiquement le carnet de terrain :

![processing_importlsci](/_static/images/fr/processing_importlsci.png)

## Options

L'import se fait en sélectionnant :

- le fichier de codification utilisé pour l'enquête terrain (`.qlsc`)
- le fichier CSV avec les données de l'enquête terrain
- le répertoire d'archivage (là où va se réaliser l'export)
- le niveau de journalisation (log)
- le fichier de journalisation (log)

![importlsci_windows](/_static/images/fr/importlsci_window.png)

## Traitement

Le plugin copie les [GeoPackage] sources dans le répertoire d'archivage. Si aucun chemin n'est indiqué QGIS crée un dossier temporaire avant d'y copier les données dedans.

LSCI[^lsci] vous propose également de logguer ces différentes étapes, particulièrement utiles pour les développeurs en cas de problème (`DEBUG`), mais également pour vous (`INFO`) pour vous indiquer ce qui a été réalisé et s'il y a des erreurs dans votre codification.

## Résultat en sortie

![importlsci_windows_result](/_static/images/fr/importlsci_window_result.png)

Les données du terrain sont chargées dans vos différents [GeoPackage] et vous pouvez admirer votre résultat en chargeant votre projet :

![result](/_static/images/result.png)

Vous avez la possibilité de refaire le traitement autant de fois que vous le souhaitez. si vous constatez une erreur, vous pouvez donc corriger manuellement votre dessin ou reprendre votre fichier terrain et le réimporter.

```{note}
Comme le plugin est intégré dans QGIS et notamment les traitements, il est possible de chaîner l'import avec d'autres traitements.
```

----

## Principe du traitement du CSV vers vos GeoPackage

Cette partie détaille le fonctionnement technique du plugin. Elle n'est pas nécessaire pour l'utiliser.

### Séparation des codes

L’outil va séparer les codes qui sont sur un même point, pour ensuite les apparier suivant leur code. Par exemple un CSV comme :

```csv
2,1980244.900,5190520.938,1002.461,300+200
3,1980249.438,5190515.953,1002.329,101
1,1980242.941,5190519.460,1002.521,200+300
```

sera transformé en :

```csv
    2,1980244.900,5190520.938,1002.461,300
    2,1980244.900,5190520.938,1002.461,200
    3,1980249.438,5190515.953,1002.329,101
    1,1980242.941,5190519.460,1002.521,200
    1,1980242.941,5190519.460,1002.521,300
```

puis :

```csv
    3,1980249.438,5190515.953,1002.329,101
    2,1980244.900,5190520.938,1002.461,200
    1,1980242.941,5190519.460,1002.521,200
    2,1980244.900,5190520.938,1002.461,300
    1,1980242.941,5190519.460,1002.521,300
```

L’outil traite les codes à la file, une attention particulière est demandée à l’opérateur saisissant les codes car une erreur de saisie décalera la codification et le dessin en résultant.

### Conversion des points en géométries

Une fois le CSV traité en interne, le plugin va le lire séquentiellement et convertit les points en géométries suivant le code.

### Format

À ce jour, le plugin ne sait lire que les fichiers de points au format CSV. Celui-ci est souvent présent sur les appareils. Ils devront respecter cette disposition :

`numéro,x,y,z,code,att1,att2, [...] attn`

où :

- `numéro` est le numéro du point,
- `x`, `y` et `z` les coordonnées,
- `code` le(s) code(s) sur le point,
- `att1`, `att2` et `attn` sont les attributs pouvant être exportés de votre carnet de terrain. Il n'y a pas de limite sur le nombre d'attribut.

<!-- Footnotes -->
[^lsci]: acronyme du nom du plugin Land Survey Codes Import

<!-- Hyperlinks reference -->
[GeoPackage]: ../glossary.html#geopackage

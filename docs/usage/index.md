# Fonctionnement

Le principe général est le suivant :

1. [Préparer les données de terrain](data_preparation)
2. [Concevoir la codification](codification)
3. Mener son étude de terrain
4. [Importer les relevés automatiquement](import_landsurvey)

```{eval-rst}
.. mermaid::

    flowchart TD
        A[Préparation des données] -->|Conversion en GeoPackage| B
        B{Création de la codification} --> C{Relevés sur le terrain}
        C -->|Conversion du shape| D[Format CSV LSCI]
        C -->|Conversion du Trimble JobXML| D
        C -->|Conversion du CSV constructeur| D
```

## Etapes

```{toctree}
---
maxdepth: 1
---
data_preparation
codification
import_landsurvey
```

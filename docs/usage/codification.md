# Éditeur de codification

L’éditeur va vous permettre de réaliser la codification utilisée lors de votre relevé topographique.
Les géométries résultantes de votre traitement seront dans des fichiers [GeoPackage] que vous aurez préalablement créés.
Vous pouvez pré-remplir des attributs dès cette configuration.

![codification_window](/_static/images/fr/codification_main.png)

Le fichier de configuration est un simple fichier texte au format YAML : [QLSC] (extension `.qlsc`).
Vous pouvez réaliser autant de codifications que vous le souhaitez.

## Pré-requis

La préparation de la codification ne peut se faire sans avoir au préalable ouvert un projet QGIS contenant les couches dans lesquelles les données seront insérées.
Ces couches doivent être dans des [GeoPackage]. Il n’y a pas de limitation quant aux nombres de ces [GeoPackage].
Une bonne pratique est de regrouper les couches dans des [GeoPackage] thématiques : eau, assainissement, voirie, etc.
Si la donnée est dans une base PostgreSQL, il est possible de les exporter dans des [GeoPackage] via l’outil « Empaquetage de couche » ou le plugin [exportDBMapper](https://gitlab.com/Oslandia/qgis/exportdbmapper/).

Vous trouverez des cas d'utilisations dans la [section préaparation des données](data_preparation)

En cliquant sur l’icône ![icon](/_static/images/icon.svg), la fenêtre de configuration de la codification apparaît.

![codification](/_static/images/fr/codification_main.png)

----

## Description du formulaire

### Paramètres généraux

Les paramètres généraux définissent les séparateurs utilisés :

- un séparateur pour indiquer les différents codes sur un même point
- un autre pour indiquer les paramètres.

Ceux-ci doivent être adaptés en fonction des capacités du carnet de terrain.

### Codification

#### Code

`Code` peut être alphanumérique, aucune restriction n’est imposée, mais il faut qu’il soit compatible avec votre appareil.

```{warning}
Il est préférable de ne pas insérer d’espaces ou de caractères spéciaux.
```

#### Description

Le champ `Description` permet d’ajouter un commentaire sur le code.

#### Géométrie

La `Géométrie` est à choisir parmi celle indiquée ci-avant. Elle est filtrée sur la géométrie de la « Couche de sortie » depuis le « [GeoPackage] » sélectionné en dessous.

----

### Attributs

Vous avez la possibilité de générer automatiquement des attributs dans les colonnes de la couche en utilisant les mêmes principes que le calculateur d’expression de QGIS. Attention, toutefois celui-ci n’a pas vocation à accepter des requêtes complexes, il est préférable de réaliser cette étape après l’import des données.

Si les données exportées du carnet possèdent des attributs supplémentaires, il est possible de les intégrer en spécifiant dans le champ expression `_attN` où `N` correspond au numéro du champ (en commençant par 1).

Il n’y a pas de limitation sur les attributs tant que le fichier CSV est conforme.

----

### Points spéciaux

Les points en erreur peuvent être ajoutés dans une couche particulière tout comme l’ensemble des points relevés.

```{note}
On appelle "en erreur", les points qui ne sont pas conformes à la règle de codification, par exemple code devant avoir 3 points et n’ayant que 2 points, etc.
```

----

### Ouverture / Enregistrement

![codification_file](/_static/images/fr/codification_file.png)

Le menu Fichier permet l’ouverture, fermeture, enregistrement, etc. de la codification.

> Voir également [la spécification du format QLSC](../format_qlsc).

<!-- Hyperlinks -->
[GeoPackage]: ../glossary
[QLSC]: ../format_qlsc

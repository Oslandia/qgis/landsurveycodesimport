# Questions fréquentes

## Le CSV n'est pas compatible avec celui de LSCI ?

Aucun problème, vous pouvez le convertir en utilisant QGIS ou de nombreux outils.

Si vous avez déjà développé vos scripts ou modèles QGIS, ou si vous souhaitez nous financer pour le faire, [contactez-nous](mailto:infos@oslandia.com?subject=LSCI%20plugin).

## Quelle différence avec des solutions comme Input ou QField ?

[Input] et [QField] sont deux solutions embarquées permettant de relever et éditer des données sur le terrain. Ils nécessitent l'installation d'une application sur une tablette ou smartphone. LSCI[^lsci] ne nécessite que l'utilisation du [carnet de terrain] qui équipe votre appareil de topographie (station totale ou GNSS) comme un Trimble TSC3, Leica CS20, etc.

Ce plugin s'adresse aux brigades de topographie habituée à ce genre de levé codifié ou ceux soucieux d'optimiser le relevé de terrain grâce à cette méthode.

<!-- Footnotes -->
[^dao]: Dessin Assisté par Ordinateur
[^lsci]: acronyme du nom du plugin Land Survey Codes Import

<!-- Hyperlinks reference -->
[carnet de terrain]: glossary
[Input]: https://inputapp.io/
[QField]: https://qfield.org/

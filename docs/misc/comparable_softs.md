# Outils similaires

Le plugin a un champ fonctionnel partiellement similaire ou équivalent à :

- [Covadis](https://www.geo-media.com/solutions/logiciel-covadis/topographie) (GeoMedia)
- [Mensura](https://www.geomensura.fr/logiciels/mensura-light/topographie-de-terrain)
- [TcpMDT Surveying](https://www.aplitop.com/produits/topographie-mdt)
- [TopStation](https://www.jsinfo.fr/topstation/applicatifs/codification-cartographique/6)

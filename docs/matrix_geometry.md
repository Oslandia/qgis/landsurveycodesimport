# Table de correspondance des éléments par types de géométrie

L’outil propose de générer les éléments suivant pour chaque type de géométrie :

| Type | Point | Ligne | Polygone | Nombre de points | Nombre de Paramètres |
| :--- | :---: | :--: | :-----: | ---------------- | -------------------- |
| Cercle par 2 points | X | X | X | 2 | 0 |
| Cercle par 3 points | X | X | X | 3 | 0 |
| Cercle par le centre et le rayon[^1] | X | X | X | 1 | 1 |
| Cercle par le centre et le diamètre[^1] | X | X | X | 1 | 1 |
| Carré par 2 points | X | X | X | 2 | 0 |
| Carré par 2 points en diagonale | X | X | X | 2 | 0 |
| Rectangle par 2 points et la hauteur[^1] | X | X | X | 2 | 1 |
| Rectangle par 3 points (3ème point = distance depuis le 2ème point) | X | X | X | 3 | 0 |
| Rectangle par 3 points (3ème point = projeté orthogonale) | X | X | X | 3 | 0 |
| Ligne[^2] | | X | X | Minimum 2 | 1 |
| Point | X | | | 1 | 0 |


## Illustration des géométries supportées

### Cercle par 2 points

![circle_2_points](../_static/images/geometry/circle_by_2_points.svg "Cercle par 2 points")

### Cercle par 3 points

![circle_3_points](../_static/images/geometry/circle_by_3_points.svg "Cercle par 3 points")

### Cercle par le centre et le rayon

![circle_center_radius](../_static/images/geometry/circle_by_center_and_radius.svg "Cercle par le centre et le rayon")

### Cercle par le centre et le diamètre

![cercle_center_diameter](../_static/images/geometry/circle_by_center_and_diameter.svg "Cercle par le centre et le diamètre")

### Carré par 2 points

![square_2_points](../_static/images/geometry/square_by_2_points.svg "Carré par 2 points")

### Carré par 2 points en diagonale

![square_diagonal_points](../_static/images/geometry/square_by_2_diagonal_points.svg "Carré par 2 points en diagonale")

### Rectangle par 2 points et la hauteur

![rectangle_2_points](../_static/images/geometry/rectangle_by_2_points_and_height.svg "Rectangle par 2 points et la hauteur")

### Rectangle par 3 points (3ème point = distance depuis le 2ème point)

![rectangle_distance](../_static/images/geometry/rectangle_by_3_points_3rd_point__distance_from_2nd_point.svg "Rectangle par 3 points (3ème point = distance depuis le 2ème point)")

### Rectangle par 3 points (3ème point = projeté orthogonale)

![rectangle_projection](../_static/images/geometry/rectangle_by_3_points_3rd_point__orthogonal_projection.svg "Rectangle par 3 points (3ème point = projeté orthogonale)")

### Ligne

![line](../_static/images/geometry/line.svg "Ligne")

### Point

![point](../_static/images/geometry/point.svg "Point")

----

<!-- Footnotes reference -->

[^1]: Signifie que le code doit avoir un paramètre et que celui-ci est de type numérique. Exemple du cercle par un centre et le rayon. Vous renseignez le centre du cercle par un point dont le code est 100 et vous devez renseigner le rayon (dans l’unité de la projection) en accolant le caractère de paramètre et la mesure, soit 100-1 pour un cercle d’un mètre de rayon.

[^2]: une ligne est traitée de façon spéciale puisqu’elle a besoin d’une information indiquant l’ouverture et la fermeture de la ligne, ainsi que des changements de nature (arcs). En prenant pour exemple, une ligne avec un code de 100, cette ligne a 4 points doit être renseignée de cette façon :
    - 1, [...], 100-1
    - 2, [...], 100-2
    - 3, [...], 100-2
    - 4, [...], 100-9

    Pour une ligne, les paramètres sont:

    - 1 : commencer une nouvelle ligne
    - 2 : continuer la ligne avec un segment de droite
    - 3 : ajouter un point sur un arc. Vous devez avoir au moins 3 points continues avec ce paramètre pour obtenir un arc par 3 points. Vous pouvez prendre pour exemple test_line.csv for detailled example.
    - 9 : terminer la ligne.

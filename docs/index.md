# {{ title }} - Documentation

> **Description :** {{ description }}  
> **Auteurs et contributeurs :** {{ author }}  
> **Version du plugin :** {{ version }}  
> **Version minimum de QGIS :** {{ qgis_version_min }}  
> **Version maximum de QGIS :** {{ qgis_version_max }}  
> **Code source :** {{ repo_url }}  
> **Dernière mise à jour de la documentation :** {{ date_update }}

---

## Pourquoi ce plugin ?

Le plugin LSCI[^lsci] (_Land Survey Codes Import_, traduit Codification Topographique) permet de gérer dans QGIS les levés codifiés au retour des enquêtes terrain réalisées par les topographes.

## Comment fonctionne la codification topographique dans un SIG ?

La logique d'utilisation de LSCI[^lsci] est proche de n'importe quel outil de ce type sur un logiciel de DAO[^dao].

Il est composé :

- d'un [formulaire de configuration de votre codification](usage/codification)
- d'un [outil d'import du carnet de terrain](usage/import_landsurvey)
- d'[outils pour traiter vos données](tools/index).

L'intégration dans QGIS et notamment dans la boîte à outils de traitements ouvre la voie à l'utilisation automatisée dans les chaînes de traitements des utilisateurs finaux.

### Ce que permet la topographie en SIG par rapport à la DAO

Si l'on considère que le SIG est du DAO[^dao] avec des attributs, on obtient un
plan de plus grande qualité graphique et avec une plus grande richesse
d'information.
La gestion des règles de géométrie et de topologie font qu'un plan réalisé avec
des outils SIG sera plus qu'un dessin.

Réaliser votre plan de topographie directement dans un SIG comme QGIS vous
permettra :
- d'ajouter des attributs à votre dessin.
    - le plugin gère l'ajout d'attributs selon vos codes grâce à son intégration complète dans QGIS ;
- import et export facilité au format SIG et DAO ;
- enchaînement des traitements (ex: import du carnet de terrain, puis enchaînement de la vérification de la qualité du plan).

### Ce que ne permet pas le plugin

Le plugin n'est pas un outil de topométrie. Vous devrez réaliser vos calculs au
préalable.

Nous travaillons actuellement sur une solution de ce type. N'hésitez pas à nous
[contacter](mailto:infos@oslandia.com) si vous êtes intéressés par le sujet.

----

```{toctree}
---
caption: Table des matières
maxdepth: 3
---
installation
quickstart
usage/index
tools/index
format_qlsc
matrix_geometry
glossary
misc/comparable_softs
misc/faq
```

---

```{toctree}
---
caption: Guide de contribution
maxdepth: 1
---
Documentation du code<_apidoc/modules>
development/contribute
development/add_new_geometry_type
development/environment
development/documentation
development/packaging
development/testing
development/sponsors
development/history
```

---

```{toctree}
---
caption: Références externes
maxdepth: 1
---
Billet de blog présentant la codification et le plugin <https://oslandia.com/2020/02/07/la-codification-des-leves-de-topographie/>
Le plugin sur le dépôt officiel de QGIS <https://plugins.qgis.org/plugins/LandSurveyCodesImport/>
```

<!-- Footnotes -->
[^dao]: Dessin Assisté par Ordinateur
[^lsci]: acronyme du nom du plugin Land Survey Codes Import

<!-- Hyperlinks reference -->
[carnet de terrain]: glossary
[Input]: https://inputapp.io/
[QField]: https://qfield.org/

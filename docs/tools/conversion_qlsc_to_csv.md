# Conversion des fichiers de codification QLSC en CSV

Le format [QLSC] a été développé pour répondre aux besoins de flexibilité des usages du plugin mais ne constitue pas une fin en soi ou un [mécanisme de verrouillage](https://fr.wikipedia.org/wiki/Enfermement_propri%C3%A9taire).
Ainsi, le plugin intègre un outil de conversion des fichiers QLSC en CSV, un format standardisé et pleinement interopérable avec de nombreux outils.

![processing_qlsc2csv](/_static/images/fr/processing_qlsc2csv.png)

## Options

Dans ce traitement, il est possible de :

- sélectionner le fichier [QLSC] à exporter
- choisir les paramètres de la codification à exporter. Chaque élément correspond à une colonne du tableur en sortie.
- où stocker le fichier de sortie (en mémoire ou sur le disque)

![qlsc2csv_window](/_static/images/fr/qlsc2csv_window.png)

## Résultat en sortie

![qlsc2pdf_window_result](/_static/images/fr/lsci_export_csv_result_calc.png)

<!-- Hyperlinks reference -->
[QLSC]: ../format_qlsc

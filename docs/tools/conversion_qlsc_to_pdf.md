# Export des fichiers de codification QLSC au format PDF

Afin de faciliter l'utilisation des fichiers de codification dans les enquêtes terrain, le plugin intègre un outil de conversion des fichiers QLSC en un tableau en PDF, un format lisible dans de nombreux outils et permettant par exemple d'imprimer facilement la codification.

![processing qlsc2pdf](/_static/images/fr/processing_qlsc2pdf.png)

Sous le capot, ce traitement utilise d'abord [l'export de la codification en CSV](conversion_qlsc_to_csv) avant d'imprimer le tableau en sortie à l'aide du composeur d'impression QGIS, via un projet QGIS.

```{note} Pourquoi un projet plutôt que des modèles de composition ?
À l'époque du développement initial, c'était alors le seul mécanisme stable disponible dans toutes les versions de QGIS.
Envie de moderniser ce mécanisme ? Pourquoi ne pas [proposer voire financer le développement de cette idée](/development/sponsors) ?
```

## Options

Dans ce traitement, il est possible de :

- sélectionner le fichier [QLSC] à exporter
- sélectionner le projet QGIS à utiliser pour l'export
- le nom de la mise en page à utiliser pour l'export
- où stocker le fichier de sortie (en mémoire ou sur le disque)

![qlsc2pdf_window](/_static/images/fr/qlsc2pdf_window.png)

## Résultat en sortie

![qlsc2pdf result](/_static/images/fr/lsci_export_pdf_result.png)

## Ressources

Un projet QGIS contenant des modèles d'impressions est livré avec le plugin : `processing/export/codificationLayoutTemplate/project.qgz`. Aperçu de la mise en page simple :

![qlsc2pdf map composer](/_static/images/fr/lsci_export_pdf_map_composer.png)

<!-- Hyperlinks reference -->
[QLSC]: ../format_qlsc

# Conversion des fichiers Trimble JobXML en CSV

Les carnets de terrains Trimble sont équipés d'un mode permettant d'exporter dans un format CSV compatible avec celui du plugin LSCI[^lsci].
Néanmoins, le plugin intègre un traitement permettant de convertir les fichiers Trimble JobXML en CSV.

![processing_jobxml2csv](/_static/images/fr/processing_jobxml2csv.png)

## Options

- sélectionner le fichier JobXML
- `Utiliser seulement...`: permet de sélectionner uniquement les points principaux (non supprimés et avec les informations essentielles)
- `Exporter également les points même supprimés`. Cette option ne peut être activée que si la précédente ne l'est pas
- où stocker le fichier de sortie (en mémoire ou sur le disque)

![jxl2csv_window](/_static/images/fr/jxl2csv_window.png)

## Résultat en sortie

![jxl2csv_window_result](/_static/images/fr/jxl2csv_window_result.png)

## Ressources

Un fichier exemple est disponible sur le [dépôt de code du plugin](https://gitlab.com/Oslandia/qgis/landsurveycodesimport/-/raw/master/tests/fixtures/codification.jxl).

<!-- Footnotes -->
[^lsci]: acronyme du nom du plugin Land Survey Codes Import

<!-- Hyperlinks reference -->
[QLSC]: ../format_qlsc

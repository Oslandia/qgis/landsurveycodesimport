# Outils de traitement fournis avec le plugin

En plus de la gestion de la codifiation, différents utilitaires sont fournis avec le plugin sous forme de traitements QGIS.
Ils sont donc accessibles dans [la boîte à outils des traitements](https://docs.qgis.org/3.16/fr/docs/user_manual/processing/toolbox.html) intégrée via le menu `Traitement` du menu horizontal de QGIS.

![QGIS Processing toolbox](/_static/images/fr/qgis_processing_toolbox_lsci.png "QGIS Processing Toolbox - Land Survey Codes Import")

Par commodité, les outils sont également accessibles directement depuis les menus du plugin :

![QGIS menu plugin LSCI](/_static/images/fr/lsci_menu.png "menu du plugin QGIS LSCI")

## Outils

```{toctree}
---
maxdepth: 1
---
conversion_TrimbleJobXML_to_csv
conversion_qlsc_to_csv
conversion_qlsc_to_pdf
```

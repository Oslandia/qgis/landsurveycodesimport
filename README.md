# Land Survey Codes Import - QGIS plugin

[![pipeline status](https://gitlab.com/Oslandia/qgis/landsurveycodesimport/badges/master/pipeline.svg)](https://gitlab.com/Oslandia/qgis/landsurveycodesimport/-/commits/master)
[![documentation badge](https://img.shields.io/badge/documentation-autobuilt%20with%20Sphinx-blue)](https://oslandia.gitlab.io/qgis/landsurveycodesimport/)
[![pylint](https://oslandia.gitlab.io/qgis/landsurveycodesimport/lint/pylint.svg)](https://oslandia.gitlab.io/qgis/landsurveycodesimport/lint/)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

## Land Survey QGIS plugin

This is a plugin for QGIS to import codified topographic surveys. It consists of a codification editor and a processing in the toolbox.

[:gb: Check-out the documentation - :fr: Consulter la documentation](https://oslandia.gitlab.io/qgis/landsurveycodesimport/)

![Land Survey Codes Import - UI](docs/_static/images/en/codification_main.png "Land Survey Codes Import - UI")

## Translations

The translations for the documentation are handled with [weblate](https://weblate.org)

#! python3  # noqa E265

"""
    Usage from the repo root folder:

    .. code-block:: bash
        # for whole tests
        python -m unittest tests.test_qlsc_reader
        # for specific test
        python -m unittest tests.test_qlsc_reader.TestQlscReader.test_qlsc_reader
"""

# standard library
import unittest
from pathlib import Path
from tempfile import mkdtemp

# 3rd party
import yaml

# project
from LandSurveyCodesImport import __about__
from LandSurveyCodesImport.toolbelt.qlsc_reader import QlscYamlReader

# ############################################################################
# ########## Classes #############
# ################################


class TestQlscReader(unittest.TestCase):
    """Test module."""

    # -- Standard methods --------------------------------------------------------
    @classmethod
    def setUpClass(cls):
        """Executed when module is loaded before any test."""
        fixtures_dir = Path("tests/fixtures/")
        cls.fixtures_qlsc = fixtures_dir.glob("**/*.qlsc")

    def test_qlsc_reader(self):
        """Test QLSC reader and validator agins fixtures."""
        for i in self.fixtures_qlsc:
            # testing with a Path
            QlscYamlReader(i)

            # testing with a bytes object
            with i.open("rb") as in_yaml:
                QlscYamlReader(in_yaml)

    def test_qlsc_path_replacer(self):
        """Test path replacer."""
        tmp_dir = Path(mkdtemp(prefix=f"{__about__.__title_clean__}_TESTS_"))
        codif_filepath_in = Path(
            "LandSurveyCodesImport/resources/demo/codification.qlsc"
        )
        codif_filepath_out = tmp_dir / "codification.qlsc"

        # testing with a Path
        qlsc_rdr_in = QlscYamlReader(codif_filepath_in)
        yaml_replaced = qlsc_rdr_in.replace_parent_paths("/tmp/lsci", str(tmp_dir))
        self.assertIsInstance(yaml_replaced, dict)

        with codif_filepath_out.open(mode="w") as file_stream:
            yaml.dump(yaml_replaced, file_stream)

        # read and validate output file
        QlscYamlReader(codif_filepath_out)

    def test_qlsc_as_dict(self):
        """Test QLSC as dict."""
        for i in self.fixtures_qlsc:
            # testing with a Path
            qlsc_reader = QlscYamlReader(i)

            self.assertIsInstance(qlsc_reader.as_dict, dict)


# ############################################################################
# ####### Stand-alone run ########
# ################################
if __name__ == "__main__":
    unittest.main()

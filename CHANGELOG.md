# Changelog

## [0.3.0-beta7] - 2021/06/24

- Fixes logger error
- Adds a new processing to export fieldbook in a PostgreSQL DB (This tool requires ExportDBMapper plugin)
- Improved documentation

## [0.3.0-beta6] - 2021/04/28

- Fixes missing resources_rc

## [0.3.0-beta5] - 2021/04/28

- Improved documentation
- Improved logging function
- Adds demonstration and quickstart
- Adds UI shortcuts
- Fixes processing translation

## [0.3.0-beta3] - 2021/02/09

- Fix automatic deployment from GitLab CI

## [0.3.0-beta2] - 2021/02/08

- Documentation refactoring
- Use modern workflow to package and release the plugin
- Minor code changes

## [0.3.0-beta1] - 2020/12/23

- Improve code quality
- Adds an archiving function: Now, the plugin no longer works directly on the input data but makes a copy. This avoids having to modify the input data.
- Adds logging function (optional)

### 0.2.2 - 2020/08/26

- Add the ability to generate a minimal bouding geometry (convex, oriented minimum bounding box and minimal circle) from all points

## 0.2.1 - 2020/02/10

- Export LandSurveyCodesImport codification file (.qlsc) to PDF
- Improve Trimble JobXML to csv (Use all fields, can export deleted points)

## 0.2.0 - 2020/08/19

- Add support to arc for line
- Add support to rectangle/square => Minimal version is now QGIS 3.6
- Import Trimble JobXML to LandSurveyCodesImport csv
- Export LandSurveyCodesImport codification file (.qlsc) to csv

## 0.1.1 - 2020/07/15

- Fix incorrect ownership of algorithms in processing provider
- Remove experimental flag

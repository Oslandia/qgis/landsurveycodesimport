#!-*- coding:utf-8 -*-

"""
/***************************************************************************
                                 A QGIS plugin
 This plugin allows you to easily import data from a land survey (GPS or
 total station) to draw automatically in a database using a codification
 (aka Field Codes).
                             -------------------
        begin                : 2018-04-05
        git sha              : $Format:%H$
        copyright            : (C) 2018 by Loïc Bartoletti (Oslandia)
        email                : loic.bartoletti@oslandia.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
import os

from LandSurveyCodesImport.available_codes import AVAILABLE_CODE


def checkFile(fnm, write: bool = False):
    """
    Convenient method to check the existence of a file.
    """
    flag = os.R_OK
    if write:
        flag = os.W_OK
    if os.path.exists(fnm):
        if os.path.isfile(fnm):
            return os.access(fnm, flag)
        return False
    if not write:
        return False
    # target does not exist, check perms on parent dir
    pdir = os.path.dirname(fnm)
    if not pdir:
        pdir = "."
    # target is creatable if parent dir is writable
    return os.access(pdir, flag)


def verifyCodification(code_file):
    """
    Convenient method to ensure the code_file is valid.

    The structure of a codification is:
    ```
    AllPoints: {Layer: <GEOPACKAGE_PATH>|layername=<LAYERNAME>,
    isChecked: <true|false>}
    CodeSeparator: <A CHARACTER>
    Codification:
    '<CODE>':
        Attributes:
        - !!python/tuple [<COLUMN_NAME>, <EXPRESSION or special value>]
        Description: '<A DESCRIPTION>'
        GeometryType: '<GEOMTERYTYPE FROM THE VALID TYPE>'
        Layer:<GEOPACKAGE_PATH>|layername=<LAYERNAME>
    ErrorPoints: {Layer: <GEOPACKAGE_PATH>|layername=<LAYERNAME>,
    isChecked: <true|false>}
    BoundingGeometry:
        BoundingGeometryType: <CODE_GEOMETRY>
        Layer:<GEOPACKAGE_PATH>|layername=<LAYERNAME>
        isChecked: true
    ParameterSeparator: '<A CHARACTER>'
    ```
    Parameters
    ----------
    code_file : the yaml file loaded
    """
    assert all(
        [
            x in code_file
            for x in [
                "AllPoints",
                "ErrorPoints",
                "ParameterSeparator",
                "CodeSeparator",
                "Codification",
            ]
        ]
    )

    for c in ["CodeSeparator", "ParameterSeparator"]:
        sep = code_file[c]
        # TODO: Code and Parameter check in a predefined list?
        assert isinstance(sep, str) and len(sep) == 1

    for c in ["AllPoints", "ErrorPoints"]:
        dict_points = code_file[c]
        assert (
            "Layer" in dict_points
            and isinstance(dict_points["Layer"], str)
            and
            # TODO: check if layer exists
            "isChecked" in dict_points
            and isinstance(dict_points["isChecked"], bool)
        )

    codif = code_file["Codification"]
    assert isinstance(codif, dict)
    # assert(len(codif.keys()) > 0)
    for k in codif.keys():
        c = codif[k]
        assert all(
            [x in c for x in ["Attributes", "Description", "GeometryType", "Layer"]]
        )
        assert isinstance(c["Attributes"], list)
        assert isinstance(c["Description"], str)
        assert isinstance(c["GeometryType"], str) and c["GeometryType"] in [
            v["code"] for v in AVAILABLE_CODE()
        ]
        assert (
            isinstance(c["Layer"], str)
            # TODO: check if layer exists
        )

"""
/***************************************************************************
                                 A QGIS plugin
 This plugin allows you to easily import data from a land survey (GPS or
 total station) to draw automatically in a database using a codification
 (aka Field Codes).
                             -------------------
        begin                : 2021-06-24
        git sha              : $Format:%H$
        copyright            : (C) 2021 by Loïc Bartoletti (Oslandia)
        email                : loic.bartoletti@oslandia.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
import processing
from PyQt5.QtCore import QCoreApplication
from qgis.core import (
    QgsProcessing,
    QgsProcessingAlgorithm,
    QgsProcessingMultiStepFeedback,
    QgsProcessingParameterBoolean,
    QgsProcessingParameterFile,
    QgsProcessingParameterFileDestination,
    QgsProcessingParameterFolderDestination,
)


class LSCI2PG(QgsProcessingAlgorithm):
    """
    Import csv file with a codification to a postgresql with ExportDBmapper
    plugin.
    """

    def initAlgorithm(self, config=None):
        """
        Here we define the inputs and output of the algorithm, along
        with some other properties.
        """
        self.addParameter(
            QgsProcessingParameterFile(
                "codification",
                "codification",
                behavior=QgsProcessingParameterFile.File,
                fileFilter="QLSC Files (*.qlsc)",
                defaultValue=None,
            )
        )
        self.addParameter(
            QgsProcessingParameterFile(
                "fieldbookcsv",
                "fieldbook csv",
                behavior=QgsProcessingParameterFile.File,
                fileFilter="Fichiers CSV (*.csv)",
                defaultValue=None,
            )
        )
        self.addParameter(
            QgsProcessingParameterFile(
                "GPKGtoPGYAML",
                "GPKG to PG YAML",
                behavior=QgsProcessingParameterFile.File,
                fileFilter="YAML Files (*.yaml)",
                defaultValue=None,
            )
        )
        self.addParameter(
            QgsProcessingParameterFolderDestination(
                "Archive", "archive", createByDefault=True, defaultValue=None
            )
        )
        self.addParameter(
            QgsProcessingParameterFileDestination(
                "Log",
                "log",
                optional=True,
                fileFilter="log",
                createByDefault=True,
                defaultValue=None,
            )
        )

    def processAlgorithm(self, parameters, context, model_feedback):
        """
        Here is where the processing itself takes place.
        """
        # Use a multi-step feedback, so that individual child algorithm progress reports are adjusted for the
        # overall progress through the model
        feedback = QgsProcessingMultiStepFeedback(2, model_feedback)
        results = {}
        outputs = {}

        # Import du carnet de terrain
        alg_params = {
            "INPUT": parameters["fieldbookcsv"],
            "LEVELLOG": 3,
            "QLSC": parameters["codification"],
            "DEST": parameters["Archive"],
            "LOG": parameters["Log"],
        }
        outputs["ImportDuCarnetDeTerrain"] = processing.run(
            "landsurvey:importlsci",
            alg_params,
            context=context,
            feedback=feedback,
            is_child_algorithm=True,
        )
        results["Archive"] = parameters["Archive"]
        results["Log"] = parameters["Log"]

        feedback.setCurrentStep(1)
        if feedback.isCanceled():
            return {}

        # Export gpkg to pg
        alg_params = {"INPUT": parameters["GPKGtoPGYAML"], "TRUNC": False}
        outputs["ExportGpkgToPg"] = processing.run(
            "ExportDBMapper:Exportgpkg2pg",
            alg_params,
            context=context,
            feedback=feedback,
            is_child_algorithm=True,
        )
        return results

    def name(self):
        """
        Returns the algorithm name, used for identifying the algorithm. This
        string should be fixed for the algorithm, and must not be localised.
        The name should be unique within each provider. Names should contain
        lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return "importlsci2pg"

    def displayName(self):
        """
        Returns the translated algorithm name, which should be used for any
        user-visible display of the algorithm name.
        """
        return self.tr(
            "Import land survey fields into PostgreSQL (requires ExportDBMapper plugin activated)"
        )

    def group(self):
        """
        Returns the name of the group this algorithm belongs to. This string
        should be localised.
        """
        return self.tr("Land Survey")

    def groupId(self):
        """
        Returns the unique ID of the group this algorithm belongs to. This
        string should be fixed for the algorithm, and must not be localised.
        The group id should be unique within each provider. Group id should
        contain lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return "landsurvey"

    def tr(self, message):
        """
        Translation method.
        """
        return QCoreApplication.translate(self.__class__.__name__, message)

    def createInstance(self):
        """
        Create instance.
        """
        return LSCI2PG()

"""
/***************************************************************************
                                 A QGIS plugin
 This plugin allows you to easily import data from a land survey (GPS or
 total station) to draw automatically in a database using a codification
 (aka Field Codes).
                             -------------------
        begin                : 2018-04-05
        git sha              : $Format:%H$
        copyright            : (C) 2018 by Loïc Bartoletti (Oslandia)
        email                : loic.bartoletti@oslandia.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

# pylint: disable=import-error
from qgis.core import (
    Qgis,
    QgsCircularString,
    QgsCompoundCurve,
    QgsCurvePolygon,
    QgsGeometry,
    QgsLineString,
    QgsMessageLog,
    QgsPoint,
)


def lineFromPoints(points, layerType, parameters):
    """
    Returns a line from the points

    Parameters
    ----------

    points: list of coordinates
        Contains at least one point
    parameters: list
        Codification for the special points on a line:
            3. Arc (3 points consecutively must have this code)
    layerType: int
        QGIS identifiant of the layer type
        (0: Point, 1: LineString, 2: Polygon)
    """
    if layerType == 0:
        return None

    nb = len(points)

    if nb < 2:
        return (None, nb)

    line = []
    arc = []
    error = []
    arcBefore = False
    curve = QgsCompoundCurve()
    for i, p in enumerate(points):
        if parameters[i] != "3":
            if len(arc) > 0:  # Push arc and invalid point in line
                while len(arc) != 3 and len(arc) > 3:
                    a = arc.pop(0)
                    b = arc.pop(0)
                    c = arc[0]
                    curve.addCurve(QgsCircularString(a, b, c))
                if len(arc) == 3:
                    curve.addCurve(QgsCircularString(*arc))
                    arc = []
                else:
                    QgsMessageLog.logMessage(
                        "Points converted to line:", level=Qgis.Warning
                    )
                    QgsMessageLog.logMessage(str(arc), level=Qgis.Warning)
                    error += arc
                    line += arc
                arc = []
            # The final point, is a single point and must be a linestring,
            # so we take the last point
            if (len(line) == 0 and i + 1 == len(points)) or arcBefore:
                line.append(QgsPoint(*[float(f) for f in points[i - 1]]))
            line.append(QgsPoint(*[float(f) for f in p]))
            arcBefore = False
        else:
            # get first point to the junction
            if len(line) > 0:
                line.append(QgsPoint(*[float(f) for f in p]))
                curve.addCurve(QgsLineString(line))
                line = []
            arc.append(QgsPoint(*[float(f) for f in p]))
            arcBefore = True

    if arc:
        while len(arc) != 3 and len(arc) > 3:
            a = arc.pop(0)
            b = arc.pop(0)
            c = arc[0]
            curve.addCurve(QgsCircularString(a, b, c))
        if len(arc) == 3:
            curve.addCurve(QgsCircularString(*arc))
            arc = []
        else:
            QgsMessageLog.logMessage("Points converted to line:", level=Qgis.Warning)
            QgsMessageLog.logMessage(str(arc), level=Qgis.Warning)
            error += arc
            line += arc
        arc = []

    if line:  # Oh, only a linestring not yet parsed
        curve.addCurve(QgsLineString(line))

    if layerType == 1:
        return (QgsGeometry(curve), error)
    p = QgsCurvePolygon()
    curve.close()
    p.setExteriorRing(curve)
    return (QgsGeometry(p), error)

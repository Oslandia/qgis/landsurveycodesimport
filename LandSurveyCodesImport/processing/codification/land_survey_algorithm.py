"""
/***************************************************************************
                                 A QGIS plugin
 This plugin allows you to easily import data from a land survey (GPS or
 total station) to draw automatically in a database using a codification
 (aka Field Codes).
                             -------------------
        begin                : 2018-04-05
        git sha              : $Format:%H$
        copyright            : (C) 2018 by Loïc Bartoletti (Oslandia)
        email                : loic.bartoletti@oslandia.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
# This will get replaced with a git SHA1 when you do a git archive

__revision__ = "$Format:%H$"

import logging

from PyQt5.QtCore import QCoreApplication

# pylint: disable=import-error
from qgis.core import (
    QgsProcessingAlgorithm,
    QgsProcessingParameterEnum,
    QgsProcessingParameterBoolean,
    QgsProcessingParameterFile,
    QgsProcessingParameterFileDestination,
    QgsProcessingParameterFolderDestination,
)

from ...land_survey_utils import checkFile
from .land_survey_field_codes_import import landsurveyImport


class landsurveyAlgorithm(QgsProcessingAlgorithm):
    """
    Import csv file with a codification to a geopackage.
    """

    # Constants used to refer to parameters and outputs. They will be
    # used when calling the algorithm from another algorithm, or when
    # calling from the QGIS console.

    QLSC = "QLSC"
    INPUT = "INPUT"
    DEST = "DEST"
    LOG = "LOG"
    LEVELLOG = "LEVELLOG"
    LEVEL = [
        ("CRITICAL", logging.CRITICAL),
        ("ERROR", logging.ERROR),
        ("WARNING", logging.WARNING),
        ("INFO", logging.INFO),
        ("DEBUG", logging.DEBUG),
    ]

    def initAlgorithm(self, config):
        """
        Here we define the inputs and output of the algorithm, along
        with some other properties.
        """

        self.addParameter(
            QgsProcessingParameterFile(
                self.INPUT, self.tr("Input CSV"), extension="csv"
            )
        )

        self.addParameter(
            QgsProcessingParameterFile(
                self.QLSC, self.tr("QLSC file"), extension="qlsc"
            )
        )

        self.addParameter(
            QgsProcessingParameterFolderDestination(
                self.DEST, self.tr("Archive folder")
            )
        )

        self.addParameter(
            QgsProcessingParameterFileDestination(
                self.LOG, self.tr("Log file"), fileFilter="log", optional=True
            )
        )

        self.addParameter(
            QgsProcessingParameterEnum(
                self.LEVELLOG,
                self.tr("Log level"),
                list(zip(*self.LEVEL))[0],
                defaultValue=3,
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        """
        Here is where the processing itself takes place.
        """
        source = self.parameterAsFile(parameters, self.INPUT, context)
        qlscfile = self.parameterAsFile(parameters, self.QLSC, context)
        logfile = self.parameterAsFile(parameters, self.LOG, context)
        levellog = self.parameterAsEnum(parameters, self.LEVELLOG, context)
        archive = self.parameterAsFile(parameters, self.DEST, context)

        logger = None
        if logfile != "":
            fmt = (
                "[%(filename)s:%(lineno)s - "
                + "%(funcName)20s() ] %(levelname)s: %(message)s"
            )

            if checkFile(logfile, True):
                logger = logging.getLogger("LSCI")
                hdlr = logging.FileHandler(logfile)
                hdlr.setFormatter(logging.Formatter(fmt))
                logger.addHandler(hdlr)
                logger.setLevel(self.LEVEL[levellog][1])
                logger.info("START")
            else:
                raise IOError("Can not open file: {}".format(logfile))

        landsurveyImport(source, qlscfile, archive)

        if logger:
            logger.info("END")

        ret = dict()
        ret["INPUT"] = source
        ret["QLSC"] = qlscfile
        ret["LOG"] = logfile
        ret["OUTPUT"] = archive
        return ret

    # pylint: disable=no-self-use
    def name(self):
        """
        Returns the algorithm name, used for identifying the algorithm. This
        string should be fixed for the algorithm, and must not be localised.
        The name should be unique within each provider. Names should contain
        lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return "importlsci"

    def displayName(self):
        """
        Returns the translated algorithm name, which should be used for any
        user-visible display of the algorithm name.
        """
        return self.tr("Import land survey fields")

    def group(self):
        """
        Returns the name of the group this algorithm belongs to. This string
        should be localised.
        """
        return self.tr("Land Survey")

    def groupId(self):
        """
        Returns the unique ID of the group this algorithm belongs to. This
        string should be fixed for the algorithm, and must not be localised.
        The group id should be unique within each provider. Group id should
        contain lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return "landsurvey"

    def tr(self, message):
        """
        Translation method.
        """
        return QCoreApplication.translate(self.__class__.__name__, message)

    def createInstance(self):
        """
        Create instance.
        """
        return landsurveyAlgorithm()

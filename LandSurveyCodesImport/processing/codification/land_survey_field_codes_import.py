"""
/***************************************************************************
                                 A QGIS plugin
 This plugin allows you to easily import data from a land survey (GPS or
 total station) to draw automatically in a database using a codification
 (aka Field Codes).
                             -------------------
        begin                : 2018-04-05
        git sha              : $Format:%H$
        copyright            : (C) 2018 by Loïc Bartoletti (Oslandia)
        email                : loic.bartoletti@oslandia.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import csv
import logging
import shutil
from pathlib import Path

import yaml
from PyQt5.QtCore import QVariant

# pylint: disable=import-error
from qgis.core import (
    QgsExpression,
    QgsExpressionContext,
    QgsExpressionContextScope,
    QgsFeature,
    QgsField,
    QgsGeometry,
    QgsMultiPoint,
    QgsPoint,
    QgsVectorLayer,
    QgsWkbTypes,
)

from LandSurveyCodesImport.available_codes import AVAILABLE_CODE

# pylint: disable=relative-beyond-top-level
from ...land_survey_utils import verifyCodification
from .geom_from_points import (
    circle2points,
    circle3points,
    circleCenterDiameter,
    circleCenterRadius,
    rectangle2PointsHeight,
    rectangle3PointsDistance,
    rectangle3PointsProjected,
    square2Diagonal,
    square2Points,
)
from .line_from_points import lineFromPoints

try:
    yaml_load = yaml.full_load
except AttributeError:
    yaml_load = yaml.load

CODE_POSITION = 4
PARAM_POSITION = 5
ATTRS_POSITION = 6

CODES = set()

LOG = logging.getLogger("LSCI")


def geomFromType(points, parameters, geomtype, layerType):
    """
    Returns the geometry from a points list, with parameters for the specific
    geometry type to the layer type

    Parameters
    ----------

    points: list of coordinates
        Contains at least one point
    parameters: list
        Can be empty, else it's a string needed for some geometry type
    geomtype: int
        Identifiant of the type of geometry
        (Circle from 2 points, Circle from center and diameter, etc.)
    layerType: int
        QGIS identifiant of the layer type
        (0: Point, 1: LineString, 2: Polygon)
    """

    def logGeomOrNone(geom):
        """
        Convenient method to log if the method returns a true geometry or None
        if an error occured.
        """
        if geom is not None:
            LOG.debug("geom:")
            LOG.debug(geom.asWkt())
        else:
            LOG.warning("Error treating geomety conversion")

    LOG.debug("points:")
    LOG.debug(points)
    LOG.debug("parameters:")
    LOG.debug(parameters)
    LOG.debug("geomtype:")
    LOG.debug(geomtype)
    LOG.debug("layerType:")
    LOG.debug(layerType)

    if geomtype == "Circle2Points":
        LOG.debug("type: Circle2Points")
        geom = circle2points(points, layerType)
        logGeomOrNone(geom)
        return geom
    if geomtype == "Circle3Points":
        LOG.debug("type: Circle3Points")
        geom = circle3points(points, layerType)
        logGeomOrNone(geom)
        return geom
    if geomtype == "CircleCenterRadius":
        LOG.debug("type: CircleCenterRadius")
        geom = circleCenterRadius(points, layerType, parameters)
        logGeomOrNone(geom)
        return geom
    if geomtype == "CircleCenterDiameter":
        LOG.debug("type: CircleCenterDiameter")
        geom = circleCenterDiameter(points, layerType, parameters)
        logGeomOrNone(geom)
        return geom
    if geomtype == "Square2Points":
        LOG.debug("type: Square2Points")
        geom = square2Points(points, layerType)
        logGeomOrNone(geom)
        return geom
    if geomtype == "Square2Diagonal":
        LOG.debug("type: Square2Diagonal")
        geom = square2Diagonal(points, layerType)
        logGeomOrNone(geom)
        return geom
    if geomtype == "Rectangle2PointsHeight":
        LOG.debug("type: Rectangle2PointsHeight")
        geom = rectangle2PointsHeight(points, layerType, parameters)
        logGeomOrNone(geom)
        return geom
    if geomtype == "Rectangle3PointsDistance":
        LOG.debug("type: Rectangle3PointsDistance")
        geom = rectangle3PointsDistance(points, layerType)
        logGeomOrNone(geom)
        return geom
    if geomtype == "Rectangle3PointsProjected":
        LOG.debug("type: Rectangle3PointsProjected")
        geom = rectangle3PointsProjected(points, layerType)
        logGeomOrNone(geom)
        return geom
    if geomtype == "Line":
        LOG.debug("type: Line")
        geom = lineFromPoints(points, layerType, parameters)
        logGeomOrNone(geom[0])
        LOG.debug("Error: ")
        LOG.debug(geom[1])
        return geom
    if geomtype == "Point":
        LOG.debug("type: Point")
        try:
            if layerType == 0:
                geom = QgsGeometry(QgsPoint(*[float(f) for f in points[0]]))
                LOG.debug("geom:")
                LOG.debug(geom.asWkt())
                return geom
            LOG.warning("Wrong layerType for Point geometry")
            return None
        except (ValueError, IndexError):
            LOG.warning("Error treating geomety conversion")
            return None

    LOG.debug("Unhandled type")
    return None


def addRowInLayer(row, errTable, table_codif):
    """
    Rows will be converted in a geometry thanks to codification.
    All attributes will be added thanks to QgsExpressionContext.

    Parameters
    ----------
    row: list of list
        A row contains one or many list of points.
    errTable: list of list
        Contains points in error.
        Some points can be added after the end of this function.
    table_codif: dictionnary
        The codification file. See the information about qlsc format.
    """

    LOG.debug("row:")
    LOG.debug(row)
    LOG.debug("errTable")
    LOG.debug(errTable)
    LOG.debug("table_codif")
    LOG.debug(table_codif)

    #  TODO: assert?
    code = row[CODE_POSITION][0]
    parameters = row[PARAM_POSITION]

    codif = table_codif["Codification"][code]
    layerName = codif["Layer"]

    layer = QgsVectorLayer(layerName)

    dim = 4 if QgsWkbTypes.hasZ(layer.dataProvider().wkbType()) else 3

    LOG.debug("Converts row to geometry")
    geom = geomFromType(
        list(zip(*row[1:dim])),
        parameters,
        codif["GeometryType"],
        layer.geometryType(),
    )

    if codif["GeometryType"] == "Line":
        orphanNodes = geom[1]
        geom = geom[0]

    if geom:
        if codif["GeometryType"] == "Line":
            arc = list(zip(*row))
            for r in arc[len(arc) - len(orphanNodes) :]:
                LOG.warning(f"add point {r} in errTable")
                errTable.append(r)

        LOG.debug("Start editing layer")
        layer.startEditing()

        fields = layer.fields()
        newFeature = QgsFeature(fields)

        newFeature.setGeometry(geom)

        LOG.debug("Add attributes")
        for e in codif["Attributes"]:
            LOG.debug("Attribute")
            LOG.debug(e)
            if e[1].startswith("_att"):
                try:
                    nb = int(e[1][4:]) - 1
                    assert nb >= 0
                    val = row[ATTRS_POSITION + nb][0]
                    newFeature[e[0]] = val
                except AttributeError as e:
                    LOG.warning("Attribute error")
                    LOG.warning(str(e))
            else:
                context = QgsExpressionContext()
                scope = QgsExpressionContextScope()
                try:
                    exp = QgsExpression(e[1])
                    scope.setFeature(newFeature)
                    context.appendScope(scope)
                    newFeature[e[0]] = exp.evaluate(context)
                except KeyError as e:
                    LOG.warning("Key error")
                    LOG.warning(str(e))

        ret = layer.addFeature(newFeature)
        if not ret:
            LOG.warning("Feature not added.")

        ret = layer.commitChanges()
        if not ret:
            LOG.warning(layer.commitErrors())
            for err in list(zip(*row)):
                LOG.warning(f"Point in error {err}")
                errTable.append(err)

        layer.updateExtents()
    else:
        for err in list(zip(*row)):
            LOG.warning("Point not converted to a geometry.")
            LOG.warning("Add it in error table {err}")
            errTable.append(err)

    LOG.debug("errTable")
    LOG.debug(errTable)


def parseTable(table, errTable, table_codif, parameterSeparator="-"):
    """
    This is the principal function. From a separated table from ``separeTable``
    the function traverse all rows in the table, look at the code. If a code
    require several points, traverse as required to get all necessary points
    and create the desired geometry. If the number of points is not available,
    for example only two points with the code for a circle with 3 points, the
    points are sended in the error table.
    TODO: linestring

    Parameters
    ----------
    table: list of list
        The table contains survey points, separated before.
        It's a list of list with same lengths.
    errTable: list of list
        Contains points in error.
        Some points can be added after the end of this function.
    table_codif: dictionnary
        The codification file. See the information about qlsc format.
    parameterSeparator: str
        The separator for parameter

    Returns
    -------
        Nothing
        rows in error will be added in errTable

    Examples
    --------
    table = [['2', '1980244.900', '5190520.938', '1002.461', '300'],
            ['2', '1980244.900', '5190520.938', '1002.461', '200'],
            ['3', '1980249.438', '5190515.953', '1002.329', '101'],
            ['1', '1980242.941', '5190519.460', '1002.521', '200'],
            ['1', '1980242.941', '5190519.460', '1002.521', '300']]
    errTable = [['4', '1980243.653', '5190516.354', '1002.369', '42-50']]
    # Simplified for the example
    # GeometryType are:
    #     10: a point
    #      0: circle by 2 points
    #      1: circle by 3 points
    table_codif = {'Codification': {'101': {'GeometryType': 10},
                                    '200': {'GeometryType': 1},
                                    '300': {'GeometryType': 0}}}
    parseTable(table, errTable, table_codif)
    # works of addRowInLayer
    print(errTable)
    >>> [['4', '1980243.653', '5190516.354', '1002.369', '42-50'],
         ['2', '1980244.900', '5190520.938', '1002.461', '200'],
         ['1', '1980242.941', '5190519.460', '1002.521', '200']]]
    """
    av_code = AVAILABLE_CODE()

    logging.debug("table:")
    logging.debug(table)
    logging.debug("errTable:")
    logging.debug(errTable)
    logging.debug("table_codif:")
    logging.debug(table_codif)
    logging.debug("parameterSeparator:")
    logging.debug(parameterSeparator)

    # sorted is stable
    # [['3', '1980249.438', '5190515.953', '1002.329', '101'],
    #  ['2', '1980244.900', '5190520.938', '1002.461', '200'],
    #  ['1', '1980242.941', '5190519.460', '1002.521', '200'],
    #  ['2', '1980244.900', '5190520.938', '1002.461', '300'],
    #  ['1', '1980242.941', '5190519.460', '1002.521', '300']]
    w_table = sorted(table, key=lambda t: t[CODE_POSITION])

    logging.debug("sorted table:")
    logging.debug(w_table)

    j = 0
    while j < len(w_table):
        row = w_table[j]
        code = row[CODE_POSITION]
        try:
            codif = table_codif["Codification"][code]
            idx = [v["code"] for v in av_code].index(codif["GeometryType"])
            nbPoints = av_code[idx]["nbpoints"]
        except KeyError as e:
            logging.warning("Key error {}", str(e))
            break

        initRow = []
        initRow.append(row)
        n = 0
        if nbPoints < 0:  # Special case for line
            logging.debug("parse line")
            run = True
            while run:
                if j + n + 1 >= len(w_table):
                    break
                tmp_row = w_table[j + n + 1]
                tmp_code = tmp_row[CODE_POSITION]
                if tmp_code == code:
                    tsep = tmp_row[PARAM_POSITION][0]
                    if tsep == "1":
                        run = False
                    else:
                        initRow.append(tmp_row)
                        n += 1
                        if tsep == "9":
                            run = False
                else:
                    run = False
            if len(initRow) < 2:
                logging.warning("Line too short")
                for err in list(zip(*initRow)):
                    LOG.warning("Point in error")
                    LOG.debug(err)
                    errTable.append(err)
                initRow = None
        else:
            logging.debug("parse points")
            for n in range(1, nbPoints):
                # special case when a geometry require multiple points
                # and we have traverse the table
                if j + n >= len(w_table):
                    LOG.warning("Point in error")
                    LOG.debug(initRow)
                    errTable.append(initRow)
                    break
                tmp_row = w_table[j + n]
                initRow.append(tmp_row)
                if tmp_row[CODE_POSITION] != code:
                    initRow.pop()
                    n -= 1
                    LOG.warning("Point in error")
                    LOG.debug(initRow)
                    errTable.append(initRow)
                    break
        j += n
        j += 1

        # Play with me
        if initRow:
            addRowInLayer(list(zip(*initRow)), errTable, table_codif)


def insertSpecialPoints(table, layerFile):
    """
    Insert "special" points of error or all table in is specific layer.

    Parameters
    ----------
    table: list of list
        The table contains, all or in error, survey points.
    layerFile: Path to the QgsVectorLayer
        The specific layer where points will go.
    """

    LOG.debug("table:")
    LOG.debug(table)
    LOG.debug("layerFile:")
    LOG.debug(layerFile)
    layer = QgsVectorLayer(layerFile)
    if layer.geometryType() != QgsWkbTypes.PointGeometry:
        LOG.warning("Wrong geometry type")
        return

    LOG.info("Start editing")
    layer.startEditing()
    fieldsList = ["point_id", "point_x", "point_y", "point_z", "point_code"]

    LOG.info("Start editing")
    for f in fieldsList:
        if layer.fields().indexFromName(f) == -1:
            layer.dataProvider().addAttributes([QgsField(f, QVariant.String)])
    for i in range(max([len(t) for t in table]) - ATTRS_POSITION):
        f = "point_att" + str(i + 1)
        if layer.fields().indexFromName(f) == -1:
            layer.dataProvider().addAttributes([QgsField(f, QVariant.String)])

    layer.updateFields()
    fields = layer.fields()

    for row in table:
        LOG.debug("Add row in table:")
        LOG.debug(row)
        newFeature = QgsFeature(fields)
        if QgsWkbTypes.Point == layer.dataProvider().wkbType():
            wkt_point = "POINT({} {})".format(row[1], row[2])
        else:
            wkt_point = "POINT({} {} {})".format(row[1], row[2], row[3])
        newFeature.setGeometry(QgsGeometry.fromWkt(wkt_point))
        newFeature["point_id"] = row[0]
        newFeature["point_x"] = row[1]
        newFeature["point_y"] = row[2]
        newFeature["point_z"] = row[3]
        newFeature["point_code"] = row[4]
        for i in range(len(row[ATTRS_POSITION:])):
            newFeature["point_att" + str(i + 1)] = row[ATTRS_POSITION + i]
        ret = layer.addFeature(newFeature)
        if not ret:
            LOG.warning("Feature not added.")
    layer.commitChanges()


def insertBoundingGeometry(table, layerFile, boundingGeometryType):
    """
    Insert bounding geometry of points in table.

    Parameters
    ----------
    table: list of list
        The table contains, all or in error, survey points.
    layerFile: Path to the QgsVectorLayer
        The specific layer where points will go.
    boundingGeometryType: type of boundingGeometry
        (0: convex, 1: ombb, 2: circle).
    """

    LOG.debug("table:")
    LOG.debug(table)
    LOG.debug("layerFile:")
    LOG.debug(layerFile)
    LOG.debug("boundingGeometryType:")
    LOG.debug(boundingGeometryType)

    layer = QgsVectorLayer(layerFile)
    if layer.geometryType() != QgsWkbTypes.PolygonGeometry:
        LOG.warning("Wrong geometry type")
        return

    LOG.info("Start editing")
    layer.startEditing()
    maxCol = 3 if layer.dataProvider().wkbType() == QgsWkbTypes.Polygon else 4
    mp = QgsMultiPoint()
    for points in table:
        mp.addGeometry(QgsPoint(*[float(pt) for pt in points[1:maxCol]]))

    g = QgsGeometry(mp)
    boundingGeometry = QgsGeometry()

    if boundingGeometryType == 0:
        LOG.debug("Convex Hull")
        boundingGeometry = g.convexHull()
    elif boundingGeometryType == 1:
        LOG.debug("Oriented Minimum Bounding Box")
        boundingGeometry = g.orientedMinimumBoundingBox()[0]
    elif boundingGeometryType == 2:
        LOG.debug("Mininaml Enclosing Circle")
        boundingGeometry = g.minimalEnclosingCircle()[0]

    newFeature = QgsFeature(layer.fields())
    newFeature.setGeometry(boundingGeometry)
    ret = layer.addFeature(newFeature)
    if not ret:
        LOG.warning("Feature not added.")

    layer.commitChanges()


def separeTable(table, codesList, CodeSeparator="+", ParameterSeparator="-"):
    """
    From a table separate all rows with a multi code on the same point.

    Parameters
    ----------
    table : list of list
        The table contains survey points is a list of list with same lengths.
    codesList : dict_keys
        Codes allowed in the codification file. Keys are str.
    CodeSeparator : str
        Separator character to separate multi code on a point.
    ParameterSeparator : str
        Parameter character to indicate the parameter for the code.

    Returns
    -------
    tuple with two lists of lists
        First list of list is separate table
        Second list of lists is row inputed in the error table

    Examples
    --------
    >>> table = [['2', '1980244.900', '5190520.938', '1002.461', '300+200'],
                 ['3', '1980249.438', '5190515.953', '1002.329', '101'],
                 ['1', '1980242.941', '5190519.460', '1002.521', '200+300'],
                 ['4', '1980243.653', '5190516.354', '1002.369', '42-50']]
    # simplified for the example
    >>> codesList = {'101': [],
                     '200': [],
                     '300': []}.keys()
    >>> CodeSeparator = '+'
    >>> ParameterSeparator = '-'
    >>> (outTable, errTable) = separeTable(table, codesList,
            CodeSeparator, ParameterSeparator)
    >>> print(outTable)
    [['2', '1980244.900', '5190520.938', '1002.461', '300'],
     ['2', '1980244.900', '5190520.938', '1002.461', '200'],
     ['3', '1980249.438', '5190515.953', '1002.329', '101'],
     ['1', '1980242.941', '5190519.460', '1002.521', '200'],
     ['1', '1980242.941', '5190519.460', '1002.521', '300']]
    >>> print(errTable)
    [['4', '1980243.653', '5190516.354', '1002.369', '42-50']]
    """

    LOG.debug("table:")
    LOG.debug(table)
    LOG.debug("codesList:")
    LOG.debug(codesList)
    LOG.debug("CodeSeparator:")
    LOG.debug(CodeSeparator)
    LOG.debug("ParameterSeparator:")
    LOG.debug(ParameterSeparator)
    # Pre condition
    # table must be a list
    if not (
        isinstance(table, list)
        and
        # of list
        len(table) > 0
        and all([isinstance(c, list) for c in table])
        and
        # with same lenghts
        all([True if len(c) == len(table[0]) else False for c in table])
        and
        # minimal lenght 5 (id, x, y, z, codes-parameters)
        len(table[0]) >= 5
    ):
        LOG.critical("Pre condition failed: table structure")
        raise TypeError("Pre-condition failed: table structure")

    # codesList a dict_keys
    if not isinstance(codesList, type({}.keys())) and all(
        [isinstance(k, str) for k in codesList]
    ):
        LOG.critical("Pre condition failed: codesList (dict_keys)")
        raise TypeError("Pre-condition failed: codesList (dict_keys)")

    # CodeSeparator a character
    if not isinstance(CodeSeparator, str) and len(CodeSeparator) == 1:
        LOG.critical("Pre condition failed: CodeSeparator")
        raise TypeError("Pre-condition failed: CodeSeparator")
    # ParameterSeparator a character
    if not (isinstance(ParameterSeparator, str) and len(ParameterSeparator) == 1):
        LOG.critical("Pre condition failed: ParameterSeparator")
        raise TypeError("Pre-condition failed: ParameterSeparator")

    outTable = []
    noCodeTable = []
    sep = 0
    err = 0
    for row in table:
        codes = [x for x in row[CODE_POSITION].split(CodeSeparator) if x.strip()]
        for c in codes:
            sep += 1
            try:
                sp = c.split(ParameterSeparator)
                param = sp[1]
                c = sp[0]
            except IndexError:
                param = []

            CODES.add(c)
            line = row[:CODE_POSITION] + [c] + [param] + row[CODE_POSITION + 1 :]
            if c.split(ParameterSeparator)[0] in codesList:
                outTable.append(line)
            else:
                err += 1
                noCodeTable.append(line)

    LOG.debug("Post-condition:")
    LOG.debug(type(outTable))
    LOG.debug(outTable)
    LOG.debug(type(noCodeTable))
    LOG.debug(noCodeTable)
    LOG.debug("len(outTable):")
    LOG.debug(len(outTable))
    LOG.debug("len(noCodeTable):")
    LOG.debug(len(noCodeTable))

    # Post condition
    if not (
        isinstance(outTable, list)
        and isinstance(noCodeTable, list)
        and len(noCodeTable) == err
        and (len(outTable) + len(noCodeTable)) == sep
    ):
        LOG.critical("Pre condition failed: ParameterSeparator")
        raise TypeError("Pre-condition failed: ParameterSeparator")

    return (outTable, noCodeTable)


def copyGPKG(layer, dest):
    """
    Copy the layer to dest folder.
    layer must be a valid geopackage source: '/path/layer.gpkg|layername'
    returns new layer dest
    """
    idx = layer.find("|")
    if idx == -1:
        if layer != "":
            logging.warning("layer {} is not a valid geopackage file", layer)
        return ""
    source, layerName = layer.split("|")
    source = Path(source)
    dest = Path(dest)

    shutil.copy2(source, dest / source.name)

    return str(dest / source.name) + "|" + layerName


def archivage(QLSC, FILE, dest):
    """
    Function to prepare the conversion from data with from land-survey
    fieldbook to geometries with a codification template.

    Parameters
    ----------
    FILE : File from the land survey codification
    QLSC : Codification file which will used to convert points from FILE to
    geometry in a geopackage
    dest : archive folder
    """

    dest = Path(dest)
    dest.mkdir(exist_ok=True)

    qlsc_ = dest / Path(QLSC).name
    file_ = dest / Path(FILE).name
    try:
        shutil.copy2(QLSC, qlsc_)
        shutil.copy2(FILE, file_)
    except (FileNotFoundError, PermissionError) as e:
        LOG.critical(e)
        return ("", "")

    with open(qlsc_, "r") as stream:
        code = yaml_load(stream)

        for k in code.keys():
            if k == "Codification":
                for c in code[k]:
                    if "Layer" in code[k][c]:
                        code[k][c]["Layer"] = copyGPKG(code[k][c]["Layer"], dest)
            if "Layer" in code[k]:
                code[k]["Layer"] = copyGPKG(code[k]["Layer"], dest)
    try:
        with open(qlsc_, "w") as stream:
            yaml.dump(code, stream)
    except (FileNotFoundError, IsADirectoryError, PermissionError) as e:
        LOG.critical(e)
        return ("", "")

    return str(qlsc_), str(file_)


def landsurveyImport(FILE, QLSC, destArchive):
    """
    Main function to convert a land-survey fieldbook to geometries from a
    codification template.

    Parameters
    ----------
    FILE : File from the land survey codification
    QLSC : Codification file which will used to convert points from FILE to
        geometry in a geopackage
    destArchive : Folder where FILE, QLSC and GPKG from QLSC where copied to
    """
    LOG.info(f"Survey: {FILE}")
    LOG.info(f"Codification file template: {QLSC}")

    LOG.info("Archive in progress")
    qlsc, file = archivage(QLSC, FILE, destArchive)
    LOG.info(f"Survey copied: {file}")
    LOG.info(f"Codification file copied: {qlsc}")

    codesList = []
    with open(qlsc, "r") as stream:
        code = yaml_load(stream)
        verifyCodification(code)
        codesList = code["Codification"].keys()

    LOG.debug("codesList:")
    LOG.debug(codesList)

    rows = []
    with open(file, "r") as codif:
        reader = csv.reader(codif)
        rows = [r for r in reader if (len(r) >= 5 and r[0][0] != "#")]

    LOG.debug("rows:")
    LOG.debug(rows)

    LOG.info("Separate the table")
    (table, error) = separeTable(
        rows, codesList, code["CodeSeparator"], code["ParameterSeparator"]
    )

    LOG.debug("table:")
    LOG.debug(table)
    LOG.debug("error:")
    LOG.debug(error)
    LOG.info("Parse the table")
    parseTable(table, error, code)

    if code["AllPoints"]["isChecked"] and len(table) > 0:
        LOG.info("Special points treated: AllPoints")
        insertSpecialPoints(table, code["AllPoints"]["Layer"])
    if code["ErrorPoints"]["isChecked"] and len(error) > 0:
        LOG.info("Special points treated: ErrorPoints")
        insertSpecialPoints(error, code["ErrorPoints"]["Layer"])
    if (
        "BoundingGeometry" in code
        and code["BoundingGeometry"]["isChecked"]
        and len(table) > 0
    ):
        LOG.info("Bounding geometry treated")
        insertBoundingGeometry(
            table,
            code["BoundingGeometry"]["Layer"],
            code["BoundingGeometry"]["BoundingGeometryType"],
        )

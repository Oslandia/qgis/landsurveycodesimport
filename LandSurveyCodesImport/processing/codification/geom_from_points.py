"""
/***************************************************************************
                                 A QGIS plugin
 This plugin allows you to easily import data from a land survey (GPS or
 total station) to draw automatically in a database using a codification
 (aka Field Codes).
                             -------------------
        begin                : 2018-04-05
        git sha              : $Format:%H$
        copyright            : (C) 2018 by Loïc Bartoletti (Oslandia)
        email                : loic.bartoletti@oslandia.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

# pylint: disable=import-error
from qgis.core import (
    QgsCircle,
    QgsGeometry,
    QgsPoint,
    QgsQuadrilateral,
    QgsRegularPolygon,
)


def circle2points(points, layerType):
    """
    Returns a circle from two points

    Parameters
    ----------

    points: list of coordinates
        Must contain two points
    layerType: int
        QGIS identifiant of the layer type
        (0: Point, 1: LineString, 2: Polygon)
    """
    try:
        geom = QgsCircle.from2Points(
            QgsPoint(*[float(f) for f in points[0]]),
            QgsPoint(*[float(f) for f in points[1]]),
        )
        if layerType == 0:
            return QgsGeometry(geom.center())
        if layerType == 1:
            return QgsGeometry(geom.toLineString())
        return QgsGeometry(geom.toPolygon())
    except (ValueError, IndexError):
        return None


def circle3points(points, layerType):
    """
    Returns a circle from three points

    Parameters
    ----------

    points: list of coordinates
        Must contain three points
    layerType: int
        QGIS identifiant of the layer type
        (0: Point, 1: LineString, 2: Polygon)
    """
    try:
        geom = QgsCircle.from3Points(
            QgsPoint(*[float(f) for f in points[0]]),
            QgsPoint(*[float(f) for f in points[1]]),
            QgsPoint(*[float(f) for f in points[2]]),
        )
        if layerType == 0:
            return QgsGeometry(geom.center())
        if layerType == 1:
            return QgsGeometry(geom.toLineString())
        return QgsGeometry(geom.toPolygon())
    except (ValueError, IndexError):
        return None


def circleCenterRadius(points, layerType, parameters):
    """
    Returns a circle from a point and a radius

    Parameters
    ----------

    points: list of coordinates
        Must contain only one point
    parameters: list of number
        Must contain only one number. It's the radius of the circle.
    layerType: int
        QGIS identifiant of the layer type
        (0: Point, 1: LineString, 2: Polygon)
    """
    try:
        geom = QgsCircle.fromCenterDiameter(
            QgsPoint(*[float(f) for f in points[0]]),
            float(parameters[0]) * 2.0,
        )
        if layerType == 0:
            return QgsGeometry(geom.center())
        if layerType == 1:
            return QgsGeometry(geom.toLineString())
        return QgsGeometry(geom.toPolygon())
    except (ValueError, IndexError):
        return None


def circleCenterDiameter(points, layerType, parameters):
    """
    Returns a circle from a point and a diameter

    Parameters
    ----------

    points: list of coordinates
        Must contain only one point
    parameters: list of number
        Must contain only one number. It's the diameter of the circle.
    layerType: int
        QGIS identifiant of the layer type
        (0: Point, 1: LineString, 2: Polygon)
    """
    try:
        geom = QgsCircle.fromCenterDiameter(
            QgsPoint(*[float(f) for f in points[0]]), float(parameters[0])
        )
        if layerType == 0:
            return QgsGeometry(geom.center())
        if layerType == 1:
            return QgsGeometry(geom.toLineString())
        return QgsGeometry(geom.toPolygon())
    except (ValueError, IndexError):
        return None


def square2Points(points, layerType):
    """
    Returns a square from two points

    Parameters
    ----------

    points: list of coordinates
        Must contain two points
    layerType: int
        QGIS identifiant of the layer type
        (0: Point, 1: LineString, 2: Polygon)
    """
    try:
        geom = QgsRegularPolygon(
            QgsPoint(*[float(f) for f in points[0]]),
            QgsPoint(*[float(f) for f in points[1]]),
            4,
        )
        if layerType == 0:
            return QgsGeometry(geom.center())
        if layerType == 1:
            return QgsGeometry(geom.toLineString())
        return QgsGeometry(geom.toPolygon())
    except (ValueError, IndexError):
        return None


def square2Diagonal(points, layerType):
    """
    Returns a square from two points (diagonal)

    Parameters
    ----------

    points: list of coordinates
        Must contain two points
    layerType: int
        QGIS identifiant of the layer type
        (0: Point, 1: LineString, 2: Polygon)
    """
    try:
        geom = QgsQuadrilateral.squareFromDiagonal(
            QgsPoint(*[float(f) for f in points[0]]),
            QgsPoint(*[float(f) for f in points[1]]),
        )
        if layerType == 0:
            return QgsGeometry(geom.toPolygon().centroid())
        if layerType == 1:
            return QgsGeometry(geom.toLineString())
        return QgsGeometry(geom.toPolygon())
    except (ValueError, IndexError):
        return None


def rectangle2PointsHeight(points, layerType, parameters):
    """
    Returns a rectangle from two points and a height

    Parameters
    ----------

    points: list of coordinates
        Must contain two points
    parameters: list of number
        Must contain only one number. It's the height of the rectangle.
    layerType: int
        QGIS identifiant of the layer type
        (0: Point, 1: LineString, 2: Polygon)
    """
    try:
        p0 = QgsPoint(*[float(f) for f in points[0]])
        p1 = QgsPoint(*[float(f) for f in points[1]])
        azimuth = p0.azimuth(p1)
        distance = float(parameters[0])
        geom = QgsQuadrilateral(
            p0,
            p1,
            p1.project(distance, azimuth + 90.0),
            p0.project(distance, azimuth + 90.0),
        )
        if layerType == 0:
            return QgsGeometry(geom.toPolygon().centroid())
        if layerType == 1:
            return QgsGeometry(geom.toLineString())
        return QgsGeometry(geom.toPolygon())
    except (ValueError, IndexError):
        return None


def rectangle3PointsDistance(points, layerType):
    """
    Returns a rectangle from three points (third point is the distance from
    second point)

    Parameters
    ----------

    points: list of coordinates
        Must contain two points
    layerType: int
        QGIS identifiant of the layer type
        (0: Point, 1: LineString, 2: Polygon)
    """
    try:
        geom = QgsQuadrilateral.rectangleFrom3Points(
            QgsPoint(*[float(f) for f in points[0]]),
            QgsPoint(*[float(f) for f in points[1]]),
            QgsPoint(*[float(f) for f in points[2]]),
            QgsQuadrilateral.Distance,
        )
        if layerType == 0:
            return QgsGeometry(geom.toPolygon().centroid())
        if layerType == 1:
            return QgsGeometry(geom.toLineString())
        return QgsGeometry(geom.toPolygon())
    except (ValueError, IndexError):
        return None


def rectangle3PointsProjected(points, layerType):
    """
    Returns a rectangle from three points (distance is the projection of the
    third point on the segment p1 - p2)

    Parameters
    ----------

    points: list of coordinates
        Must contain two points
    layerType: int
        QGIS identifiant of the layer type
        (0: Point, 1: LineString, 2: Polygon)
    """
    try:
        geom = QgsQuadrilateral.rectangleFrom3Points(
            QgsPoint(*[float(f) for f in points[0]]),
            QgsPoint(*[float(f) for f in points[1]]),
            QgsPoint(*[float(f) for f in points[2]]),
            QgsQuadrilateral.Projected,
        )
        if layerType == 0:
            return QgsGeometry(geom.toPolygon().centroid())
        if layerType == 1:
            return QgsGeometry(geom.toLineString())
        return QgsGeometry(geom.toPolygon())
    except (ValueError, IndexError):
        return None

<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>@default</name>
    <message>
        <location filename="../available_codes.py" line="38"/>
        <source>Circle from 2 points</source>
        <translation>Cercle par 2 points</translation>
    </message>
    <message>
        <location filename="../available_codes.py" line="48"/>
        <source>Circle from 3 points</source>
        <translation>Cercle par 3 points</translation>
    </message>
    <message>
        <location filename="../available_codes.py" line="58"/>
        <source>Circle from center and radius</source>
        <translation>Cercle par le centre et le rayon</translation>
    </message>
    <message>
        <location filename="../available_codes.py" line="68"/>
        <source>Circle from center and diameter</source>
        <translation>Cercle par le centre et le diamètre</translation>
    </message>
    <message>
        <location filename="../available_codes.py" line="78"/>
        <source>Square from 2 points</source>
        <translation>Carré par 2 points</translation>
    </message>
    <message>
        <location filename="../available_codes.py" line="88"/>
        <source>Square from 2 diagonal points</source>
        <translation>Carré par 2 points en diagonale</translation>
    </message>
    <message>
        <location filename="../available_codes.py" line="98"/>
        <source>Rectangle from 2 points and height</source>
        <translation>Rectangle par 2 points et une hauteur</translation>
    </message>
    <message>
        <location filename="../available_codes.py" line="108"/>
        <source>Rectangle from 3 points (3rd point = distance)</source>
        <translation>Rectangle par 3 points (3ème point = distance)</translation>
    </message>
    <message>
        <location filename="../available_codes.py" line="118"/>
        <source>Rectangle from 3 points (3rd point = projected orthogonal)</source>
        <translation>Rectangle par 3 points (3ème point = projetée orthogonale)</translation>
    </message>
    <message>
        <location filename="../available_codes.py" line="128"/>
        <source>Line</source>
        <translation>Ligne</translation>
    </message>
    <message>
        <location filename="../available_codes.py" line="137"/>
        <source>Point</source>
        <translation>Point</translation>
    </message>
</context>
<context>
    <name>LSCI2PG</name>
    <message>
        <location filename="../processing/models/lsci_to_pg.py" line="148"/>
        <source>Import land survey fields into PostgreSQL (requires ExportDBMapper plugin activated)</source>
        <translation>Import du carnet de terrain dans PostgreSQL (nécessite l&apos;activation du plugin ExportDBMapper)</translation>
    </message>
    <message>
        <location filename="../processing/models/lsci_to_pg.py" line="157"/>
        <source>Land Survey</source>
        <translation>Codification topographique</translation>
    </message>
</context>
<context>
    <name>LandSurveyFieldCodes</name>
    <message>
        <location filename="../land_survey_field_codes.py" line="178"/>
        <source>Land Survey Codes Import</source>
        <translation>Codification topographique</translation>
    </message>
    <message>
        <location filename="../land_survey_field_codes.py" line="216"/>
        <source>&amp;Land Survey Codes Import</source>
        <translation>Codification &amp;topographique</translation>
    </message>
    <message>
        <location filename="../land_survey_field_codes.py" line="202"/>
        <source>Help</source>
        <translation>Documentation en ligne</translation>
    </message>
    <message>
        <location filename="../land_survey_field_codes.py" line="193"/>
        <source>Load demonstration data</source>
        <translation>Charger le projet de démonstration</translation>
    </message>
</context>
<context>
    <name>LandSurveyFieldCodesDialog</name>
    <message>
        <location filename="../gui/form_mainwindow_codification.py" line="358"/>
        <source>Save File</source>
        <translation>Enregistre le fichier</translation>
    </message>
    <message>
        <location filename="../gui/form_mainwindow_codification.py" line="169"/>
        <source>New codification</source>
        <translation>Nouvelle codification</translation>
    </message>
    <message>
        <location filename="../gui/form_mainwindow_codification.py" line="180"/>
        <source>Save the project?</source>
        <translation>Enregistrer le projet ?</translation>
    </message>
    <message>
        <location filename="../gui/form_mainwindow_codification.py" line="180"/>
        <source>Do you want to save the project?</source>
        <translation>Souhaitez-vous enregistrer le projet ?</translation>
    </message>
    <message>
        <location filename="../gui/form_mainwindow_codification.py" line="203"/>
        <source>Open File</source>
        <translation>Ouvrir le fichier</translation>
    </message>
    <message>
        <location filename="../gui/form_mainwindow_codification.py" line="231"/>
        <source>Error opening the file</source>
        <translation>Erreur lors de l&apos;ouverture du fichier</translation>
    </message>
    <message>
        <location filename="../gui/form_mainwindow_codification.py" line="347"/>
        <source>Error saving the file</source>
        <translation>Erreur lors de l&apos;enregistrement du fichier</translation>
    </message>
    <message>
        <location filename="../gui/form_mainwindow_codification.py" line="556"/>
        <source>Error changing the code</source>
        <translation>Erreur lors du changement de code</translation>
    </message>
    <message>
        <location filename="../gui/form_mainwindow_codification.py" line="556"/>
        <source>Can&apos;t change the code.</source>
        <translation>Impossible de changer le code.</translation>
    </message>
    <message>
        <location filename="../gui/form_mainwindow_codification.py" line="231"/>
        <source>Can&apos;t open the file: </source>
        <translation>Impossible d&apos;ouvrir le fichier : </translation>
    </message>
    <message>
        <location filename="../gui/form_mainwindow_codification.py" line="347"/>
        <source>Can&apos;t save the file: </source>
        <translation>Impossible d&apos;enregistrer le fichier : </translation>
    </message>
    <message>
        <location filename="../gui/form_mainwindow_codification.py" line="203"/>
        <source>QGIS Land Survey Code Config (*.qlsc)</source>
        <translation>QGIS Land Survey Code Config (*.qlsc)</translation>
    </message>
    <message>
        <location filename="../gui/form_mainwindow_codification.py" line="227"/>
        <source>Error occurred during QLSC loading: {}</source>
        <translation>Erreur rencontrée lors du chargement du QLSC</translation>
    </message>
    <message>
        <location filename="../gui/form_mainwindow_codification.py" line="265"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../gui/form_mainwindow_codification.py" line="265"/>
        <source>Layer {} is not loaded in QGIS</source>
        <translation>Le layer {} n&apos;est pas chargé dans QGIS</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../gui/form_mainwindow_codification.ui" line="222"/>
        <source>Codification</source>
        <translation>Codification</translation>
    </message>
    <message>
        <location filename="../gui/form_mainwindow_codification.ui" line="166"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../gui/form_mainwindow_codification.ui" line="171"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../gui/form_mainwindow_codification.ui" line="176"/>
        <source>*</source>
        <translation>*</translation>
    </message>
    <message>
        <location filename="../gui/form_mainwindow_codification.ui" line="181"/>
        <source>/</source>
        <translation>/</translation>
    </message>
    <message>
        <location filename="../gui/form_mainwindow_codification.ui" line="186"/>
        <source>:</source>
        <translation>:</translation>
    </message>
    <message>
        <location filename="../gui/form_mainwindow_codification.ui" line="191"/>
        <source>;</source>
        <translation>;</translation>
    </message>
    <message>
        <location filename="../gui/form_mainwindow_codification.ui" line="196"/>
        <source>,</source>
        <translation>,</translation>
    </message>
    <message>
        <location filename="../gui/form_mainwindow_codification.ui" line="201"/>
        <source>=</source>
        <translation>=</translation>
    </message>
    <message>
        <location filename="../gui/form_mainwindow_codification.ui" line="318"/>
        <source>Empty</source>
        <translation>Vider</translation>
    </message>
    <message>
        <location filename="../gui/form_mainwindow_codification.ui" line="336"/>
        <source>Expressions</source>
        <translation>Expressions</translation>
    </message>
    <message>
        <location filename="../gui/form_mainwindow_codification.ui" line="105"/>
        <source>Generals parameters</source>
        <translation>Paramètres généraux</translation>
    </message>
    <message>
        <location filename="../gui/form_mainwindow_codification.ui" line="209"/>
        <source>Codes separator</source>
        <translation>Séparateur des codes</translation>
    </message>
    <message>
        <location filename="../gui/form_mainwindow_codification.ui" line="158"/>
        <source>Parameters separator</source>
        <translation>Séparateur des paramètres</translation>
    </message>
    <message>
        <location filename="../gui/form_mainwindow_codification.ui" line="231"/>
        <source>Code:</source>
        <translation>Code :</translation>
    </message>
    <message>
        <location filename="../gui/form_mainwindow_codification.ui" line="248"/>
        <source>Output layer:</source>
        <translation>Couche de sortie :</translation>
    </message>
    <message>
        <location filename="../gui/form_mainwindow_codification.ui" line="349"/>
        <source>Geometry:</source>
        <translation>Géométrie :</translation>
    </message>
    <message>
        <location filename="../gui/form_mainwindow_codification.ui" line="241"/>
        <source>Delete the code</source>
        <translation>Supprimer le code</translation>
    </message>
    <message>
        <location filename="../gui/form_mainwindow_codification.ui" line="331"/>
        <source>Attributes</source>
        <translation>Attributs</translation>
    </message>
    <message>
        <location filename="../gui/form_mainwindow_codification.ui" line="255"/>
        <source>Description:</source>
        <translation>Description :</translation>
    </message>
    <message>
        <location filename="../gui/form_mainwindow_codification.ui" line="31"/>
        <source>Specials points</source>
        <translation>Points spéciaux</translation>
    </message>
    <message>
        <location filename="../gui/form_mainwindow_codification.ui" line="40"/>
        <source>All points</source>
        <translation>Tous les points</translation>
    </message>
    <message>
        <location filename="../gui/form_mainwindow_codification.ui" line="54"/>
        <source>Errors points</source>
        <translation>Points en erreur</translation>
    </message>
    <message>
        <location filename="../gui/form_mainwindow_codification.ui" line="376"/>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <location filename="../gui/form_mainwindow_codification.ui" line="391"/>
        <source>&amp;New</source>
        <translation>&amp;Nouveau</translation>
    </message>
    <message>
        <location filename="../gui/form_mainwindow_codification.ui" line="396"/>
        <source>&amp;Open...</source>
        <translation>&amp;Ouvrir...</translation>
    </message>
    <message>
        <location filename="../gui/form_mainwindow_codification.ui" line="401"/>
        <source>&amp;Save</source>
        <translation>Enregi&amp;strer</translation>
    </message>
    <message>
        <location filename="../gui/form_mainwindow_codification.ui" line="406"/>
        <source>Save as...</source>
        <translation>Enregistrer sous...</translation>
    </message>
    <message>
        <location filename="../gui/form_mainwindow_codification.ui" line="411"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <location filename="../gui/form_mainwindow_codification.ui" line="62"/>
        <source>Convex Hull</source>
        <translation>Enveloppe convexe</translation>
    </message>
    <message>
        <location filename="../gui/form_mainwindow_codification.ui" line="67"/>
        <source>Oriented Minimum Bounding Box</source>
        <translation>Emprise orientée minimale</translation>
    </message>
    <message>
        <location filename="../gui/form_mainwindow_codification.ui" line="72"/>
        <source>Minimum Enclosing Circle</source>
        <translation>Cercle englobant minimal</translation>
    </message>
    <message>
        <location filename="../gui/form_mainwindow_codification.ui" line="356"/>
        <source>Save the code</source>
        <translation>Enregistrer le code</translation>
    </message>
    <message>
        <location filename="../gui/form_mainwindow_codification.ui" line="80"/>
        <source>Minimal bounding
geometry from points</source>
        <translation>Géométrie englobante
minimale des points</translation>
    </message>
</context>
<context>
    <name>MenuTools</name>
    <message>
        <location filename="../gui/menu_tools.py" line="41"/>
        <source>&amp;Tools</source>
        <translation>&amp;Outils</translation>
    </message>
    <message>
        <location filename="../gui/menu_tools.py" line="44"/>
        <source>Related utilities</source>
        <translation>Utilitaires liés</translation>
    </message>
</context>
<context>
    <name>landsurveyAlgorithm</name>
    <message>
        <location filename="../processing/codification/land_survey_algorithm.py" line="73"/>
        <source>Input CSV</source>
        <translation>CSV en entrée</translation>
    </message>
    <message>
        <location filename="../processing/codification/land_survey_algorithm.py" line="79"/>
        <source>QLSC file</source>
        <translation>Fichier QLSC</translation>
    </message>
    <message>
        <location filename="../processing/codification/land_survey_algorithm.py" line="85"/>
        <source>Archive folder</source>
        <translation>Dossier d&apos;archive</translation>
    </message>
    <message>
        <location filename="../processing/codification/land_survey_algorithm.py" line="91"/>
        <source>Log file</source>
        <translation>Fichier de journalisation (log)</translation>
    </message>
    <message>
        <location filename="../processing/codification/land_survey_algorithm.py" line="97"/>
        <source>Log level</source>
        <translation>Niveau de journalisation</translation>
    </message>
    <message>
        <location filename="../processing/codification/land_survey_algorithm.py" line="161"/>
        <source>Import land survey fields</source>
        <translation>Import du carnet de terrain</translation>
    </message>
    <message>
        <location filename="../processing/codification/land_survey_algorithm.py" line="168"/>
        <source>Land Survey</source>
        <translation>Codification topographique</translation>
    </message>
</context>
<context>
    <name>landsurveyJXL2CSV</name>
    <message>
        <location filename="../processing/import_/trimble/land_survey_jxl2csv.py" line="68"/>
        <source>JXL file</source>
        <translation>Fichier JXL</translation>
    </message>
    <message>
        <location filename="../processing/import_/trimble/land_survey_jxl2csv.py" line="72"/>
        <source>Output CSV</source>
        <translation>CSV en sortie</translation>
    </message>
    <message>
        <location filename="../processing/import_/trimble/land_survey_jxl2csv.py" line="127"/>
        <source>Convert Trimble JobXML file to CSV file</source>
        <translation>Convertir un fichier Trimble JobXML en un fichier CSV</translation>
    </message>
    <message>
        <location filename="../processing/import_/trimble/land_survey_jxl2csv.py" line="144"/>
        <source>Import</source>
        <translation>Importer</translation>
    </message>
    <message>
        <location filename="../processing/import_/trimble/land_survey_jxl2csv.py" line="78"/>
        <source>Use only Reductions fields?</source>
        <translation>Utiliser seulement les champs Reductions ?</translation>
    </message>
    <message>
        <location filename="../processing/import_/trimble/land_survey_jxl2csv.py" line="84"/>
        <source>Export also deleted points                     (only available when Reductions is false</source>
        <translation>Exporter également les points supprimés (seulement valable si Reductions vaut Faux)</translation>
    </message>
</context>
<context>
    <name>landsurveyProvider</name>
    <message>
        <location filename="../land_survey_provider.py" line="85"/>
        <source>Land survey</source>
        <translation>Codification topographique</translation>
    </message>
</context>
<context>
    <name>landsurveyQLSC2CSV</name>
    <message>
        <location filename="../processing/export/land_survey_qlsc2csv.py" line="161"/>
        <source>All points</source>
        <translation>Tous les points</translation>
    </message>
    <message>
        <location filename="../processing/export/land_survey_qlsc2csv.py" line="161"/>
        <source>Parameter for all points</source>
        <translation>Paramètre pour tous les points</translation>
    </message>
    <message>
        <location filename="../processing/export/land_survey_qlsc2csv.py" line="174"/>
        <source>Error points</source>
        <translation>Points en erreur</translation>
    </message>
    <message>
        <location filename="../processing/export/land_survey_qlsc2csv.py" line="174"/>
        <source>Parameter for error points</source>
        <translation>Paramètre pour les points en erreur</translation>
    </message>
    <message>
        <location filename="../processing/export/land_survey_qlsc2csv.py" line="188"/>
        <source>Code separator</source>
        <translation>Séparateur de code</translation>
    </message>
    <message>
        <location filename="../processing/export/land_survey_qlsc2csv.py" line="188"/>
        <source>Parameter for code separator</source>
        <translation>Paramètre pour le séparateur de code</translation>
    </message>
    <message>
        <location filename="../processing/export/land_survey_qlsc2csv.py" line="204"/>
        <source>Parameter separator</source>
        <translation>Séparateur de paramètre</translation>
    </message>
    <message>
        <location filename="../processing/export/land_survey_qlsc2csv.py" line="204"/>
        <source>Parameter for parameter separator</source>
        <translation>Paramètre pour le séparateur de paramètre</translation>
    </message>
    <message>
        <location filename="../processing/export/land_survey_qlsc2csv.py" line="225"/>
        <source>Codification file</source>
        <translation>Fichier de codification</translation>
    </message>
    <message>
        <location filename="../processing/export/land_survey_qlsc2csv.py" line="231"/>
        <source>Output CSV</source>
        <translation>CSV en sortie</translation>
    </message>
    <message>
        <location filename="../processing/export/land_survey_qlsc2csv.py" line="322"/>
        <source>Convert QLSC file to CSV file</source>
        <translation>Convertir un fichier QLSC en un fichier CSV</translation>
    </message>
    <message>
        <location filename="../processing/export/land_survey_qlsc2csv.py" line="339"/>
        <source>Export</source>
        <translation>Exporter</translation>
    </message>
</context>
<context>
    <name>landsurveyQLSC2PDF</name>
    <message>
        <location filename="../processing/export/land_survey_qlsc2pdf.py" line="57"/>
        <source>Codification file</source>
        <translation>Fichier de codification</translation>
    </message>
    <message>
        <location filename="../processing/export/land_survey_qlsc2pdf.py" line="66"/>
        <source>Template project</source>
        <translation>Projet modèle</translation>
    </message>
    <message>
        <location filename="../processing/export/land_survey_qlsc2pdf.py" line="75"/>
        <source>Output PDF</source>
        <translation>PDF en sortie</translation>
    </message>
    <message>
        <location filename="../processing/export/land_survey_qlsc2pdf.py" line="83"/>
        <source>Layout name</source>
        <translation>Nom de la mise en page</translation>
    </message>
    <message>
        <location filename="../processing/export/land_survey_qlsc2pdf.py" line="97"/>
        <source>Cannot open the file &apos;{}&apos;</source>
        <translation>Impossible d&apos;ouvrir le fichier &apos;{}&apos;</translation>
    </message>
    <message>
        <location filename="../processing/export/land_survey_qlsc2pdf.py" line="104"/>
        <source>Cannot find layout &apos;{}&apos;</source>
        <translation>Impossible de trouver la mise en page &apos;{}&apos;</translation>
    </message>
    <message>
        <location filename="../processing/export/land_survey_qlsc2pdf.py" line="119"/>
        <source>Cannot export to pdf</source>
        <translation>Impossible d&apos;exporter le pdf</translation>
    </message>
    <message>
        <location filename="../processing/export/land_survey_qlsc2pdf.py" line="133"/>
        <source>Unsaved project:</source>
        <translation>Projet non sauvegardé:</translation>
    </message>
    <message>
        <location filename="../processing/export/land_survey_qlsc2pdf.py" line="133"/>
        <source>Save your works before run this algorithm</source>
        <translation>Enregistrez votre travail avant de lancer cet algorithme</translation>
    </message>
    <message>
        <location filename="../processing/export/land_survey_qlsc2pdf.py" line="192"/>
        <source>Cannot print layout</source>
        <translation>Impossible d&apos;imprimer la mise en page</translation>
    </message>
    <message>
        <location filename="../processing/export/land_survey_qlsc2pdf.py" line="220"/>
        <source>Convert QLSC file to PDF file</source>
        <translation>Convertir un fichier QLSC en un fichier PDF</translation>
    </message>
    <message>
        <location filename="../processing/export/land_survey_qlsc2pdf.py" line="237"/>
        <source>Export</source>
        <translation>Exporter</translation>
    </message>
</context>
</TS>

FORMS = ../gui/form_mainwindow_codification.ui

SOURCES= ../land_survey_utils.py \
    ../land_survey_provider.py \
    ../processing/import_/trimble/jxl2csv.py \
    ../processing/import_/trimble/land_survey_jxl2csv.py \
    ../processing/codification/land_survey_field_codes_import.py \
    ../processing/codification/land_survey_algorithm.py \
    ../processing/export/land_survey_qlsc2csv.py \
    ../processing/export/land_survey_qlsc2pdf.py \
    ../processing/models/lsci_to_pg.py \
    ../land_survey_field_codes.py \
    ../available_codes.py \
    ../gui/form_mainwindow_codification.py \
    ../gui/menu_tools.py

TRANSLATIONS = fr.ts

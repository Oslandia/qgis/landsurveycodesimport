#! python3  # noqa: E265

"""
    Demonstration data loader.
"""

# ############################################################################
# ########## Imports ###############
# ##################################

# Standard library
import logging
from pathlib import Path
from shutil import copy
from tempfile import mkdtemp

# 3rd party
import yaml

# PyQGIS
from qgis.core import QgsProject

# project
from LandSurveyCodesImport.__about__ import DIR_PLUGIN_ROOT, __title_clean__
from LandSurveyCodesImport.toolbelt import PlgLogger, QlscYamlReader

# ############################################################################
# ########## Globals ###############
# ##################################

logger = logging.getLogger(__name__)


# ############################################################################
# ########## Classes ###############
# ##################################


class DemonstrationLoader:
    """Demonstration data loader."""

    DEMO_FOLDER: Path = DIR_PLUGIN_ROOT / "resources/demo"
    DEMO_TMP_DIR: Path = None

    def __init__(self):
        """Instancitation logic."""
        self.log = PlgLogger().log

    def _copy_demo_to_tmp(self) -> Path:
        """copy the demonstration files into a temporary folder to avoid any problem from the end-user"""
        self.DEMO_TMP_DIR = Path(mkdtemp(prefix=f"{__title_clean__}_"))
        for demo_file in self.DEMO_FOLDER.iterdir():
            copy(demo_file, self.DEMO_TMP_DIR)

        return self.DEMO_TMP_DIR

    def load(self) -> bool:
        """Load demonstration project into QGIS.

        Returns:
            bool: if demo project has been correctly loaded
        """
        loaded = False

        # copy demonstration file into a temporary folder
        demo_qgs_filepath = self._copy_demo_to_tmp() / "demo_project.qgs"

        # check codification file
        demo_codif_filepath = self.DEMO_FOLDER / "codification.qlsc"
        qlsc_reader = QlscYamlReader(demo_codif_filepath)

        # adapt layers paths to the temporary folder
        output_codification = self.DEMO_TMP_DIR / "codification.qlsc"
        with output_codification.open(mode="w") as output_stream:
            yaml.dump(
                qlsc_reader.replace_parent_paths("/tmp/lsci", str(self.DEMO_TMP_DIR)),
                output_stream,
                encoding="UTF8",
            )

        # load demo project
        if demo_qgs_filepath.is_file():
            project = QgsProject.instance()
            qgs_read = project.read(
                filename=str(demo_qgs_filepath.resolve()),
                flags=QgsProject.FlagDontLoadLayouts,
            )

            if qgs_read:
                self.log(
                    message="Demonstration data and project correctly copy then loaded from a temporary folder: {}.".format(
                        self.DEMO_TMP_DIR
                    )
                )
                loaded = True
            else:
                self.log(
                    message="Something went wrong during demonstration project loading.",
                    log_level=2,
                    push=True,
                )

        else:
            self.log(
                message=f"Demonstration data not found: {demo_qgs_filepath}",
                log_level=2,
                push=True,
            )

        return loaded

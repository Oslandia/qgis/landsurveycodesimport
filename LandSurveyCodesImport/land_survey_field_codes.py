"""
/***************************************************************************
                                 A QGIS plugin
 This plugin allows you to easily import data from a land survey (GPS or
 total station) to draw automatically in a database using a codification
 (aka Field Codes).
                             -------------------
        begin                : 2018-04-05
        git sha              : $Format:%H$
        copyright            : (C) 2018 by Loïc Bartoletti (Oslandia)
        email                : loic.bartoletti@oslandia.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

# standard library
import inspect
import os
import os.path
import sys

# PyQGIS
from qgis.core import QgsApplication  # pylint: disable=import-error
from qgis.gui import QgisInterface
from qgis.PyQt.QtCore import QCoreApplication, QSettings, QTranslator
from qgis.PyQt.QtGui import QIcon
from qgis.PyQt.QtWidgets import QAction, QMenu
from qgis.utils import showPluginHelp

from LandSurveyCodesImport.__about__ import DIR_PLUGIN_ROOT, __title__
from LandSurveyCodesImport.gui.menu_tools import MenuTools
from LandSurveyCodesImport.logic import DemonstrationLoader

from .gui.form_mainwindow_codification import LandSurveyFieldCodesDialog
from .land_survey_provider import landsurveyProvider

cmd_folder = os.path.split(inspect.getfile(inspect.currentframe()))[0]

if cmd_folder not in sys.path:
    sys.path.insert(0, cmd_folder)


class LandSurveyFieldCodes:
    """QGIS Plugin Implementation."""

    def __init__(self, iface: QgisInterface):
        """Constructor.

        :param iface: An interface instance that will be passed to this class
            which provides the hook by which you can manipulate the QGIS
            application at run time.
        :type iface: QgisInterface
        """

        self.provider = landsurveyProvider()
        # Save reference to the QGIS interface
        self.iface = iface
        # initialize locale
        self.locale = QSettings().value("locale/userLocale")[0:2]
        locale_path = str(DIR_PLUGIN_ROOT / "i18n" / f"{self.locale}.qm")

        if os.path.exists(locale_path):
            self.translator = QTranslator()
            self.translator.load(locale_path, "LandSurveyCodesImport")
            QCoreApplication.installTranslator(self.translator)

        # Create the dialog (after translation) and keep reference
        self.dlg = LandSurveyFieldCodesDialog()

        # Declare instance attributes
        self.actions = []
        self.menu = self.tr("&Land Survey Codes Import")
        self.toolbar = self.iface.addToolBar(__title__)
        self.toolbar.setObjectName(__title__)

        # submodules
        self.demo_loader = DemonstrationLoader()

    # noinspection PyMethodMayBeStatic
    # pylint: disable=no-self-use
    def tr(self, message):
        """Get the translation for a string using Qt translation API.

        We implement this ourselves since we do not inherit QObject.

        :param message: String for translation.
        :type message: str, QString

        :returns: Translated version of message.
        :rtype: QString
        """
        # noinspection PyTypeChecker,PyArgumentList,PyCallByClass
        return QCoreApplication.translate(self.__class__.__name__, message)

    def add_action(
        self,
        icon_path,
        text,
        callback,
        enabled_flag=True,
        add_to_menu=True,
        add_to_toolbar=True,
        status_tip=None,
        whats_this=None,
        parent=None,
    ):
        """Add a toolbar icon to the toolbar.

        :param icon_path: Path to the icon for this action. Can be a resource
            path (e.g. ':/plugins/foo/bar.png') or a normal file system path.
        :type icon_path: str

        :param text: Text that should be shown in menu items for this action.
        :type text: str

        :param callback: Function to be called when the action is triggered.
        :type callback: function

        :param enabled_flag: A flag indicating if the action should be enabled
            by default. Defaults to True.
        :type enabled_flag: bool

        :param add_to_menu: Flag indicating whether the action should also
            be added to the menu. Defaults to True.
        :type add_to_menu: bool

        :param add_to_toolbar: Flag indicating whether the action should also
            be added to the toolbar. Defaults to True.
        :type add_to_toolbar: bool

        :param status_tip: Optional text to show in a popup when mouse pointer
            hovers over the action.
        :type status_tip: str

        :param parent: Parent widget for the new action. Defaults None.
        :type parent: QWidget

        :param whats_this: Optional text to show in the status bar when the
            mouse pointer hovers over the action.

        :returns: The action that was created. Note that the action is also
            added to self.actions list.
        :rtype: QAction
        """

        icon = QIcon(icon_path)
        action = QAction(icon, text, parent)
        action.triggered.connect(callback)
        action.setEnabled(enabled_flag)

        if status_tip is not None:
            action.setStatusTip(status_tip)

        if whats_this is not None:
            action.setWhatsThis(whats_this)

        if add_to_toolbar:
            self.toolbar.addAction(action)

        if add_to_menu:
            self.iface.addPluginToMenu(self.menu, action)

        self.actions.append(action)

        return action

    def initGui(self):
        """Create the menu entries and toolbar icons inside the QGIS GUI."""
        # main action
        self.add_action(
            str(DIR_PLUGIN_ROOT / "resources/images/icon.svg"),
            text=self.tr("Land Survey Codes Import"),
            callback=self.run,
            parent=self.iface.mainWindow(),
        )

        # insert tools actions
        self.tools_menu = MenuTools(self.iface.mainWindow())
        self.iface.addPluginToMenu(self.menu, self.tools_menu.act_imp_survey_csv)
        self.iface.addPluginToMenu(self.menu, self.tools_menu.act_imp_trimble_jobxml)
        self.iface.addPluginToMenu(self.menu, self.tools_menu.act_exp_csv)
        self.iface.addPluginToMenu(self.menu, self.tools_menu.act_exp_pdf)

        # demonstration action
        self.add_action(
            icon_path=QIcon(QgsApplication.iconPath("mActionPlay.svg")),
            text=self.tr("Load demonstration data"),
            callback=self.demo_loader.load,
            add_to_menu=True,
            add_to_toolbar=False,
        )

        # help action
        self.add_action(
            QIcon(QgsApplication.iconPath("mActionHelpContents.svg")),
            text=self.tr("Help"),
            callback=lambda: showPluginHelp(filename="help/index"),
            add_to_toolbar=False,
            parent=self.iface.mainWindow(),
        )

        QgsApplication.processingRegistry().addProvider(self.provider)

    def unload(self):
        """Removes the plugin menu item and icon from QGIS GUI"""
        # -- Clean up actions
        for action in self.actions:
            self.iface.removePluginMenu(self.tr("&Land Survey Codes Import"), action)
            self.iface.removeToolBarIcon(action)

        # remove the toolbar
        del self.toolbar

        QgsApplication.processingRegistry().removeProvider(self.provider)

    def run(self):
        """Run method that performs all the real work"""
        # show the dialog
        self.dlg.show()

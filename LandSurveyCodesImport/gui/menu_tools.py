#! python3  # noqa: E265

"""
    Custom menu.

    Ressources: https://realpython.com/python-menus-toolbars/#adding-help-tips-to-actions
"""

# PyQGIS
from qgis import processing
from qgis.core import QgsApplication
from qgis.PyQt.QtCore import QCoreApplication
from qgis.PyQt.QtGui import QIcon
from qgis.PyQt.QtWidgets import QAction, QMenu

# project
from LandSurveyCodesImport.__about__ import __title__
from LandSurveyCodesImport.land_survey_provider import (
    landsurveyAlgorithm,
    landsurveyJXL2CSV,
    landsurveyQLSC2CSV,
    landsurveyQLSC2PDF,
)


class MenuTools(QMenu):
    """Menu pointing to plugin processings.


    Example:

        from .menu_tools import MenuTools
        menubar.addMenu(MenuTools(self))
    """

    def __init__(self, parent=None):
        """Constructor."""
        super().__init__(parent)

        # menu definition
        self.setTitle(self.tr("&Tools"))
        self.setObjectName("LSCIToolsMenu")
        self.setToolTipsVisible(True)
        self.setToolTip(self.tr("Related utilities"))

        # build menu actions and triggers
        self.initGui()

    def initGui(self):
        """Create the menu entries and toolbar icons inside the QGIS GUI."""
        # -- Actions
        self.act_exp_csv = QAction(
            icon=QIcon(QgsApplication.iconPath("mActionAddDelimitedTextLayer.svg")),
            text=landsurveyQLSC2CSV().displayName(),
        )
        self.act_exp_pdf = QAction(
            icon=QIcon(QgsApplication.iconPath("mActionSaveAsPDF.svg")),
            text=landsurveyQLSC2PDF().displayName(),
        )
        self.act_imp_survey_csv = QAction(
            icon=QIcon(QgsApplication.iconPath("mActionSharingImport.svg")),
            text=landsurveyAlgorithm().displayName(),
        )
        self.act_imp_trimble_jobxml = QAction(
            icon=QIcon(QgsApplication.iconPath("mActionSharingImport.svg")),
            text=landsurveyJXL2CSV().displayName(),
        )

        # -- Connections
        self.act_exp_csv.triggered.connect(
            lambda: processing.createAlgorithmDialog("landsurvey:qlsc2csv").show()
        )
        self.act_exp_pdf.triggered.connect(
            lambda: processing.createAlgorithmDialog("landsurvey:qlsc2pdf").show()
        )
        self.act_imp_survey_csv.triggered.connect(
            lambda: processing.createAlgorithmDialog("landsurvey:importlsci").show()
        )
        self.act_imp_trimble_jobxml.triggered.connect(
            lambda: processing.createAlgorithmDialog("landsurvey:jxl2csv").show()
        )

        # -- Menu
        self.addAction(self.act_imp_survey_csv)
        self.addSeparator()
        self.addAction(self.act_imp_trimble_jobxml)
        self.addSeparator()
        self.addAction(self.act_exp_csv)
        self.addAction(self.act_exp_pdf)

    def tr(self, string: str) -> str:
        """Returns a translatable string with the self.tr() function.

        Args:
            string (str): text to translate

        Returns:
            str: translated text
        """
        return QCoreApplication.translate(self.__class__.__name__, string)
